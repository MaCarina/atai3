package com.shared.domain;

public enum GamePhase {
    PRE_GAME,
    SETUP,
    EXPLORATION,
    PRODUCTION,
    POST_GAME
}
