package com.visualization.domain;

import com.rcll.domain.TeamColor;
import com.shared.domain.GamePhase;
import com.shared.domain.GameState;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VisualizationGameStateImpl implements VisualizationGameState {
    String teamCyan;
    String teamMagenta;
    long pointsCyan;
    long pointsMagenta;
    long gameTimeNanoSeconds;
    GameState state;
    GamePhase phase;
    TeamColor gripsColor;
}
