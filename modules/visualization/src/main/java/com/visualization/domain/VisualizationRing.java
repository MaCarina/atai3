package com.visualization.domain;

import com.rcll.domain.MachineClientUtils;

public interface VisualizationRing {
    MachineClientUtils.RingColor getRingColor();
    int getRawMaterial();
}
