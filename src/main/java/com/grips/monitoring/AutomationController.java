package com.grips.monitoring;

import com.grips.config.RefboxConfig;
import com.grips.persistence.dao.GameStateDao;
import com.rcll.refbox.RefboxClient;
import com.shared.domain.GamePhase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.math.BigInteger;
import java.util.Optional;

@RestController
public class AutomationController {

    @Autowired
    private GameStateDao gameStateDao;

    @Autowired
    private RefboxClient refboxClient;

    @RequestMapping("/game/phase")
    public String getGamePhase() {
        return Optional.ofNullable(gameStateDao.getGamePhase().name()).orElse("NULL");
    }

    @RequestMapping("/game/time")
    public BigInteger gameTime() {
        return new BigInteger(refboxClient.getLatestGameTimeInSeconds().toString());
    }

    @RequestMapping("/game/state")
    public String getGameState() {
        return Optional.ofNullable(gameStateDao.getGameState().name()).orElse("NULL");
    }
}
