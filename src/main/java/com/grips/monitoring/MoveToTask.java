package com.grips.monitoring;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
public class MoveToTask {
    private long robotId;
    private String zone;
}
