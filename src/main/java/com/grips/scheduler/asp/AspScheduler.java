package com.grips.scheduler.asp;

import com.grips.config.ProductionTimesConfig;
import com.grips.config.RefboxConfig;
import com.grips.persistence.dao.*;
import com.grips.persistence.domain.*;
import com.grips.persistence.misc.GripsDataService;
import com.grips.robot.RobotClientWrapper;
import com.grips.scheduler.api.IScheduler;
import com.grips.scheduler.asp.dispatcher.ActionMapping;
import com.rcll.domain.MachineClientUtils;
import com.rcll.domain.Order;
import com.rcll.domain.Peer;
import com.rcll.planning.encoding.RcllCodedProblem;
import com.rcll.planning.encoding.IntOp;
import com.rcll.planning.planner.Planner;
import com.rcll.planning.planparser.TemporalEdge;
import com.rcll.planning.planparser.TemporalGraph;
import com.rcll.planning.planparser.TemporalNode;
import com.grips.scheduler.exploration.PreProductionService;
import com.rcll.refbox.RefboxClient;
import com.robot_communication.services.RobotClient;
import com.shared.domain.GamePhase;
import com.shared.domain.MachineSide;
import lombok.SneakyThrows;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.robocup_logistics.llsf_msgs.AgentTasksProtos;
import org.robocup_logistics.llsf_msgs.BeaconSignalProtos;
import org.robocup_logistics.llsf_msgs.MachineInfoProtos;
import org.springframework.beans.factory.annotation.Value;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

import static com.grips.persistence.domain.SubProductionTask.TaskState.*;

@CommonsLog
public class AspScheduler implements IScheduler {
    private final GripsDataService<ProductOrder, Long> productOrderDao;
    private final GameStateDao gameStateDao;
    private final MachineInfoRefBoxDao machineInfoRefBoxDao;
    private final SubProductionTaskDao subProductionTaskDao;
    private final RobotClient robotClient;

    private final RefboxClient refboxClient;
    private final BeaconSignalFromRobotDao beaconSignalFromRobotDao;
    private final RobotClientWrapper robotClientWrapper;
    private final RobotStartChecker robotStartChecker;

    public static final String READY_AT_OUTPUT = "READY-AT-OUTPUT";
    public static final String IDLE = "IDLE";
    public static final String DOWN = "DOWN";
    public static final String BROKEN = "BROKEN";
    public static final String PREPARED = "PREPARED";
    public static final String PROCESSING = "PROCESSING";
    public static final String PROCESSED = "PROCESSED";

    @Value("${gameconfig.planningparameters.minorderstoplan}")
    private int minOrdersToPlan;
    @Value("${gameconfig.planningparameters.numberofrobots}")
    private int numberRobots;
    @Value("${gameconfig.planningparameters.productiontime}")
    private int productiontime;

    @Value("${gameconfig.planningparameters.leavestartingarea}")
    private boolean leaveStartingZoneTask;
    private final DistanzeMatrix distanzeMatrix;

    private boolean first = true;
    private boolean firstR2 = true;
    private boolean firstR3 = true;
    RcllCodedProblem cp;
    private List<Order> goals;
    private boolean stop;

    private String movingTo[];
    private boolean robotStopped[];

    private final TemporalNode[] executing;
    private final boolean[] cleanTask;
    private final SubProductionTask[] toExecute;          //keep track of the assigned subProduction task, needed since move action are now executed explicitly
    private Timer timer;
    private TemporalGraph tgraph;
    private final KnowledgeBase kb;
    private final ActionMapping actionMapping;
    private Boolean starting;

    private final RefboxConfig refboxConfig;
    private final ProductionTimesConfig productionTimesConfig;

    public AspScheduler(RobotClient robotClient, RefboxClient refboxClient,
                        GripsDataService<ProductOrder, Long> productOrderDao, GameStateDao gameStateDao, MachineInfoRefBoxDao machineInfoRefBoxDao,
                        SubProductionTaskDao subProductionTaskDao, BeaconSignalFromRobotDao beaconSignalFromRobotDao,
                        RobotClientWrapper robotClientWrapper, PreProductionService preProductionService,
                        DistanzeMatrix distanzeMatrix, KnowledgeBase kb,
                        RobotStartChecker robotStartChecker, ActionMapping actionMapping, RefboxConfig refboxConfig, ProductionTimesConfig productionTimesConfig) {
        this.productOrderDao = productOrderDao;
        this.kb = kb;
        this.robotClient = robotClient;
        this.refboxClient = refboxClient;
        this.gameStateDao = gameStateDao;
        this.machineInfoRefBoxDao = machineInfoRefBoxDao;
        this.subProductionTaskDao = subProductionTaskDao;
        this.beaconSignalFromRobotDao = beaconSignalFromRobotDao;
        this.distanzeMatrix = distanzeMatrix;
        this.robotStartChecker = robotStartChecker;
        this.actionMapping = actionMapping;
        this.refboxConfig = refboxConfig;
        this.productionTimesConfig = productionTimesConfig;
        executing = new TemporalNode[3];
        robotStopped = new boolean[3];
        toExecute = new SubProductionTask[3];
        movingTo = new String[3];
        robotStopped[0] = true;
        robotStopped[1] = true;
        robotStopped[2] = true;

        starting = true;
        stop = false;
        this.robotClientWrapper = robotClientWrapper;
        cleanTask = new boolean[3];
        cleanTask[0] = false;
        cleanTask[1] = false;
        cleanTask[2] = false;
    }


    synchronized
    private void tryDispathecLcked(TemporalEdge e) {
        tgraph.updateEdge(tgraph.getSource(e), tgraph.getTarget(e), e);
        tryDispatchNode(tgraph.getTarget(e));
    }

    @SneakyThrows
    public void calculatePlan(boolean simplestPlan) {
        if (!simplestPlan) {
            this.kb.generateInitialKB();
            this.distanzeMatrix.generateDistanceMatrix();
        }

        //START THREAD
        PlanningThread winningThread = new PlanningThread("1",  800,
                simplestPlan, kb, numberRobots, productionTimesConfig, machineInfoRefBoxDao,
                this.refboxConfig.getTeamcolor(), distanzeMatrix.getDistanzMatrix(), refboxClient);
        winningThread.plan();
        //END THREAD


        if (winningThread != null) {
            ImmutablePair<List<Order>, ImmutablePair<TemporalGraph, RcllCodedProblem>> winningResult;
            winningResult = winningThread.getResult();
            updateWinningResult(winningResult);

            kb.setCp(cp);
            kb.generateOrderKb(this.goals);     //the information about the product we planned for are added to the KB only after a plan is found
        } else {
            calculatePlan(true);
        }
    }

    private void updateWinningResult(ImmutablePair<List<Order>, ImmutablePair<TemporalGraph, RcllCodedProblem>> winningResult) {
        this.goals = winningResult.left;
        this.tgraph = winningResult.right.left;
        this.cp = winningResult.right.right;
    }

    @Override
    public void handleBeaconSignal(BeaconSignalProtos.BeaconSignal  beacon_signal, int robotId, Peer robot) {
        synchronized (this) {

            if (first && refboxClient.getAllOrders().size() >= minOrdersToPlan && robotId == 1) {
                first = false;
                calculatePlan(false);
                this.actionMapping.updateCodedProblem(cp.getConstants());
                dispatch();
            }
        }
    }

    @Override
    public boolean handleRobotTaskResult(AgentTasksProtos.AgentTask prsTask) {
if (prsTask.hasRetrieve() ) {
        return this.handleGetResult(prsTask.getTaskId(), prsTask.getRobotId(), prsTask.getSuccessful());
        } else if (prsTask.hasDeliver()) {
        return this.handleDeliverResult(prsTask.getTaskId(), prsTask.getRobotId(), prsTask.getSuccessful());
        }  else if (prsTask.hasMove()) {
        return this.handleMoveToWaypointResult(prsTask.getTaskId(), prsTask.getRobotId(), prsTask, prsTask.getSuccessful());
        } else if (prsTask.hasBuffer()) {
        return this.handleBufferCapStationResult(prsTask.getTaskId(), prsTask.getRobotId(), prsTask.getBuffer(), prsTask.getSuccessful());
        } else {
            log.error("Unsupported midlevelTaskResult: " + prsTask.toString());
            return false;
        }
    }

    private boolean handleBufferCapStationResult(int taskId, int robotId, AgentTasksProtos.BufferStation bufferCapStation, boolean wasSucess) {
        SubProductionTask subTask = subProductionTaskDao.findByIdAndRobotId(taskId, robotId);
        if (subTask == null) {
            log.error("Error, task with result was not found!");
            return false;
        } else if (wasSucess) {
            subTask.setState(SUCCESS, "robot said MoveToTask was SUCCESS");
            subProductionTaskDao.save(subTask);
            log.error("MoveTo Task: " + taskId + " was finished by robot: " + robotId);

            log.warn("SUCCESS FOR A BUFFER TASK");

            Set<TemporalEdge> outEdges = tgraph.getOutputEdges(executing[robotId - 1]);
            Iterator<TemporalEdge> it = outEdges.iterator();
            while (it.hasNext()) {
                TemporalEdge e = it.next();
                if (e.getType() == 2) {
                    e.enable();
                    tgraph.updateEdge(tgraph.getSource(e), tgraph.getTarget(e), e);
                    dispatchEndNode(tgraph.getTarget(e));
                }
            }

        } else {
            log.error("MoveTo Buffer Failed result: " + wasSucess + " for task: " + taskId + " unassigning it and retrying it...");
            subTask.setState(TBD, "Retrying after FAIL");
            subProductionTaskDao.save(subTask);             //ATAI: HERE REPLANNING AFTER FAILURE CAN BE CALLED

        }
        return true;
    }

    private boolean handleMoveToWaypointResult(Integer taskId, Integer robotId, AgentTasksProtos.AgentTask moveToWaypoint, boolean wasSucess) {
        SubProductionTask subTask = subProductionTaskDao.findByIdAndRobotId(taskId, robotId);
        if (subTask == null) {
            log.error("Error, task with result was not found!");
            return false;
        }

        if (wasSucess) {
            handleSuccessfulMoveTaskResult(taskId, robotId, subTask);
        } else {
            handleFailedMoveTaskResult(taskId, robotId, moveToWaypoint, wasSucess, subTask);
        }
        return true;
    }

    private void handleFailedMoveTaskResult(Integer taskId, Integer robotId, AgentTasksProtos.AgentTask moveToWaypoint, boolean wasSucess, SubProductionTask subTask) {
        log.error("MoveTo Task Failed result: " + wasSucess + " for task: " + taskId + " unassigning it and retrying it...");
        subTask.setState(TBD, "Retrying after FAIL");

        subProductionTaskDao.save(subTask);
        if (!cleanTask[robotId - 1]) {
            log.warn("Reassigning MOVE task");

            BeaconSignalFromRobot robotPos = beaconSignalFromRobotDao.findFirstByRobotIdOrderByLocalTimestampDesc(Integer.toString(robotId));

            String colorprefix = refboxClient.isCyan() ? "C_" : "M_";
            double startingPoint = 4.2;
            if (leaveStartingZoneTask && robotPos.getPoseY() < 1 && ((robotPos.getPoseX() < -4.2 && colorprefix.equals("M_")) || (robotPos.getPoseX() > 4.2 && colorprefix.equals("C_")))) {
                log.warn("Reassigning MOVE task to leave starting area");
                robotClient.sendPrsTaskToRobot(moveToWaypoint);
            } else {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                try {
                    robotClientWrapper.sendProductionTaskToRobot(subTask);
                } catch (Exception e) {
                    log.warn("cleanup task succeded, ignore previous log");
                }
            }
        }
        synchronized (this) {
            if (cleanTask[robotId - 1]) {
                log.warn("cleanup task failed, but probably the robot made the old position free");
                cleanTask[robotId - 1] = false;


                robotStopped[robotId - 1] = true;
                log.info("SETTING ROBOT STOPPED " + robotId + " TO TRUE INSIDE cleantaskterminated");
            }
        }
    }

    private void handleSuccessfulMoveTaskResult(Integer taskId, Integer robotId, SubProductionTask subTask) {
        subTask.setState(SUCCESS, "robot said MoveToTask was SUCCESS");
        subProductionTaskDao.save(subTask);
        log.info("MoveTo Task: " + taskId + " was finished by robot: " + robotId);

        if (robotId == 2 && firstR2 && leaveStartingZoneTask) {
            firstR2 = false;
            log.info("robot 2 exited the starting zone");
            robotStopped[robotId - 1] = true;
            log.info("SETTING ROBOT STOPPED " + robotId + " TO TRUE INSIDE waitingtaskfinished");
            Set<TemporalEdge> outEdges = tgraph.getOutputEdges(tgraph.getNode(-1));
            Iterator<TemporalEdge> it = outEdges.iterator();
            while (it.hasNext()) {
                TemporalEdge e = it.next();
                e.enable();
                tryDispathecLcked(e);
            }
        } else if (robotId == 3 && firstR3 && leaveStartingZoneTask) {
            firstR3 = false;
            log.info("robot 3 exited the starting zone");
            robotStopped[robotId - 1] = true;
            log.info("SETTING ROBOT STOPPED " + robotId + " TO TRUE INSIDE waitingtaskfinished");
            Set<TemporalEdge> outEdges = tgraph.getOutputEdges(tgraph.getNode(-2));
            Iterator<TemporalEdge> it = outEdges.iterator();
            while (it.hasNext()) {
                TemporalEdge e = it.next();
                e.enable();
                tryDispathecLcked(e);
            }
        } else if (cleanTask[robotId - 1]) {
            synchronized (this) {
                if (cleanTask[robotId - 1]) {
                    log.warn("cleanup task cancelled");
                    cleanTask[robotId - 1] = false;
                    robotStopped[robotId - 1] = true;
                    log.info("SETTING ROBOT STOPPED " + robotId + " TO TRUE INSIDE cleantaskend");
                }
            }
        } else if (toExecute[robotId - 1] != null) {
            assignNonMoveTaskToRobot(toExecute[robotId - 1], robotId);
        } else {
            Set<TemporalEdge> outEdges = tgraph.getOutputEdges(executing[robotId - 1]);
            Iterator<TemporalEdge> it = outEdges.iterator();
            while (it.hasNext()) {
                TemporalEdge e = it.next();
                if (e.getType() == 2) {
                    e.enable();
                    tgraph.updateEdge(tgraph.getSource(e), tgraph.getTarget(e), e);
                    dispatchEndNode(tgraph.getTarget(e));
                }
            }
        }
    }

    public boolean handleDeliverResult(long taskId, int robotId, boolean wasSuccess) {
        SubProductionTask subTask = subProductionTaskDao.findByIdAndRobotId(taskId, robotId);

        if (subTask == null) {
            log.error("Error, task that should fail was not found!");
            return false;
        } else if (wasSuccess) {

            subTask.setState(SUCCESS_PENDING, "robot said task was SUCCESS");
            subProductionTaskDao.save(subTask);
            log.info("Deliver Task: " + taskId + " was supposedly finished by robot: " + robotId);


            // no prepare required
            if (subTask.getMachine().contains("RS")) {
                subTask.setState(SUCCESS, "deliver was done to slide");
                subProductionTaskDao.save(subTask);
            }

            if (subTask.getMachine().contains("DS")) {
                subTask.setState(SUCCESS, "deliver was done to slide");
                subProductionTaskDao.save(subTask);
                ProductOrder order = productOrderDao.findById(subTask.getOrderInfoId()).get();
                order.setDelivered(true);
                productOrderDao.save(order);

            }


            log.warn("SUCCESS FOR A DELIVERY TASK");

            Set<TemporalEdge> outEdges = tgraph.getOutputEdges(executing[robotId - 1]);
            Iterator<TemporalEdge> it = outEdges.iterator();
            while (it.hasNext()) {
                TemporalEdge e = it.next();
                if (e.getType() == 2) {
                    e.enable();
                    tgraph.updateEdge(tgraph.getSource(e), tgraph.getTarget(e), e);
                    dispatchEndNode(tgraph.getTarget(e));
                }
            }
        } else {
            log.error("Deliver Failed result: " + wasSuccess + " for task: " + taskId + " unassigning it and retrying it...");
            subTask.setState(TBD, "Retrying after FAIL");
            subProductionTaskDao.save(subTask);     //ATAI: HERE REPLANNING AFTER FAILURE CAN BE CALLED
        }
        return true;
    }


    public boolean handleGetResult(long taskId, int robotId, boolean wasSuccess) {
        SubProductionTask subTask = subProductionTaskDao.findByIdAndRobotId(taskId, robotId);

        if (subTask == null) {
            log.error("Error, task that should fail was not found!");
            return false;
        } else if (wasSuccess) {
            subTask.setState(SUCCESS, "robot said SUCCESS");
            log.warn("SUCCESS FOR GET task" + taskId);
            subProductionTaskDao.save(subTask);


            Set<TemporalEdge> outEdges = tgraph.getOutputEdges(executing[robotId - 1]);
            Iterator<TemporalEdge> it = outEdges.iterator();
            while (it.hasNext()) {
                TemporalEdge e = it.next();
                if (e.getType() == 2) {
                    e.enable();
                    tgraph.updateEdge(tgraph.getSource(e), tgraph.getTarget(e), e);
                    dispatchEndNode(tgraph.getTarget(e));
                }
            }

        } else {
            log.error("GET Failed result: " + wasSuccess + " for task: " + taskId + ", replanning");
            subTask.setState(TBD, "Retrying after FAIL");
            subProductionTaskDao.save(subTask);                     //ATAI: HERE REPLANNING AFTER FAILURE CAN BE CALLED
        }
        return true;
    }




    //call to trigger replan after a product is lost



    @Override
    public void handleMachineInfo(MachineInfoProtos.Machine machine) {
        List<SubProductionTask> task = subProductionTaskDao.findByMachineAndState(machine.getName(), SubProductionTask.TaskState.SUCCESS_PENDING);
        for (SubProductionTask subProductionTask : task) {
            MachineClientUtils.MachineState state = refboxClient.getStateForMachine(MachineClientUtils.parseMachineWithColor(subProductionTask.getMachine())).get();
            if (state.equals(MachineClientUtils.MachineState.READY_AT_OUTPUT)) {
                subProductionTaskDao.save(subProductionTask);
            }
        }
    }


    public void assignTaskToRobot(SubProductionTask task, int robotId) {

        SubProductionTask moveTask = SubProductionTaskBuilder.newBuilder()              //ATAI: creation of move task
                .setName("MoveToMachine " + task.getMachine())
                .setMachine(task.getMachine())
                .setState(SubProductionTask.TaskState.INWORK)
                .setType(SubProductionTask.TaskType.MOVE)
                .setSide(task.getSide())
                .setOrderInfoId(null)
                .setIsDemandTask(task.isDemandTask())
                .build();

        if (moveTask.getSide() == MachineSide.SHELF || moveTask.getSide() == MachineSide.SLIDE)
            moveTask.setSide(MachineSide.INPUT);


        toExecute[robotId - 1] = task;
        moveTask.setRobotId(robotId);
        subProductionTaskDao.save(moveTask);

        synchronized (this) {
            if (cleanTask[robotId - 1]) {
                cleanTask[robotId - 1] = false;
                robotClient.cancelTask(robotId);
                try {
                    Thread.sleep(7000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        String locName = moveTask.getMachine().toLowerCase(Locale.ROOT).substring(2);
        if (!locName.equals("bs") && !locName.equals("ds")) {
            locName = locName + "_" + moveTask.getSide().toString().toLowerCase(Locale.ROOT);
        }

        robotClientWrapper.sendProductionTaskToRobot(moveTask);

        if (starting)
            starting = false;
    }




    public void assignNonMoveTaskToRobot(SubProductionTask task, int robotId) {
        log.info("TASK TO EXECUTE BY ROBOT ID " + robotId + ", checking station state");
        String colorprefix = refboxClient.isCyan() ? "C-" : "M-";
        task.setState(SubProductionTask.TaskState.INWORK, "set to INWORK due to assigning task to robot");
        subProductionTaskDao.save(task);
        robotClientWrapper.sendProductionTaskToRobot(task);
    }



    public ImmutablePair<TemporalGraph, RcllCodedProblem> getPlan(Planner pl, String threadNumber) {
        Set<Double> keys = pl.getPlan().actions().keySet();
        Iterator<Double> itk = keys.iterator();
        while (itk.hasNext()) {
            Set<IntOp> ita = pl.getPlan().getActionSet(itk.next());
            Iterator<IntOp> acs = ita.iterator();
            while (acs.hasNext()) {
                IntOp o = acs.next();
                o.generateLists();
            }
        }

        if (pl.getPlan().size() == 0) {
            log.warn("Plan not found, problem unsolvable");
            return null;
        } else {
            pl.printPlan();
            log.warn("Generating temporal graph \n");
            //plan is represented as a temporal graph inside tg object
            TemporalGraph tg = new TemporalGraph(cp, pl.getPlan(), refboxClient);
            tg.createTemporalGraph();
            tg.normalizeActions();
            //tg.printOrderedActions();
            //tg.printGraph();
            log.warn("finish");
            return new ImmutablePair<>(tg, cp);
        }
    }

    public void dispatch() {
        timer = new Timer();

        //get the plan starting node
        TemporalNode planStart = tgraph.getNode(0);
        tryDispatchNode(planStart);

    }

    public void tryDispatchNode(TemporalNode node) {
        //checking input edges are all enabled
        if (!stop) {
            boolean ready = true;
            Set<TemporalEdge> inEdges = tgraph.getInputEdges(node);
            Iterator<TemporalEdge> it = inEdges.iterator();
            while (it.hasNext() && ready) {
                TemporalEdge e = it.next();
                if (!e.getEnabled()) {
                    ready = false;
                }
            }
            if (ready && node.getType() != 2) {
                if (node.getType() != 3) {
                    synchronized (this) {
                        SubProductionTask task = actionMapping.actionToTask(tgraph.nodeToAction(node.getTime(), node.getId()));
                        int robotId = task.getRobotId();
                        robotStopped[robotId - 1] = false;
                        log.info("SETTING ROBOT STOPPED " + robotId + " TO FALSE INSIDE tryDispatchNode");
                    }
                }
                //dispatching the node
                dispatchNode(node);
                //enabling outpud edges
                Set<TemporalEdge> outEdges = tgraph.getOutputEdges(node);
                it = outEdges.iterator();
                while (it.hasNext()) {
                    TemporalEdge e = it.next();
                    if (e.getLb() == Double.MIN_VALUE) {              //simplest case, without lower bound on the edge
                        e.enable();
                        tgraph.updateEdge(tgraph.getSource(e), tgraph.getTarget(e), e);
                        tryDispatchNode(tgraph.getTarget(e));
                    } else {                                        //action duration is the only case for the moment
                        //timer.schedule(new ActionEnd(e), (System.currentTimeMillis() - timerStarting) + Math.round(e.getLb()*1000));
                        timer.schedule(new ActionEnd(e), Math.round(e.getLb() * 100));
                    }
                }

            }
        }
    }

    public void dispatchEndNode(TemporalNode node) {
        if (node.isValid()) {
            boolean ready = true;
            Set<TemporalEdge> inEdges = tgraph.getInputEdges(node);
            Iterator<TemporalEdge> it = inEdges.iterator();
            while (it.hasNext() && ready) {
                TemporalEdge e = it.next();
                if (!e.getEnabled()) {
                    ready = false;
                }
            }
            if (ready) {
                double time = node.getTime();
                IntOp action = tgraph.nodeToAction(time, node.getId());
                log.info("Time: " + time + " Timer: " + getLatestProductionTime() + " - Finishing action " + action);
                //ATAI: CHECK THE PRECONDITIONS ON THE KB HERE
                //ATAI: APPLY THE EFFECTS ON THE KB HERE
                //enabling outpud edges
                SubProductionTask task = actionMapping.actionToTask(action);
                int robotId = task.getRobotId();

                if (!stop) {
                    synchronized (this) {
                        robotStopped[robotId - 1] = true;
                        log.info("SETTING ROBOT STOPPED " + robotId + " TO TRUE INSIDE dispatchEndNode");
                    }
                    Set<TemporalEdge> outEdges = tgraph.getOutputEdges(node);
                    it = outEdges.iterator();
                    while (it.hasNext()) {
                        TemporalEdge e = it.next();
                        if (e.getLb() == Double.MIN_VALUE) {              //simplest case, without lower bound on the edge
                            e.enable();
                            tryDispathecLcked(e);
                        } else {                                        //action duration is the only case for the moment
                            timer.schedule(new ActionEnd(e), Math.round(e.getLb() * 10));
                        }
                    }

                    if (robotStopped[0] && robotStopped[1] && robotStopped[2]) {
                        log.info("Plan dispatching end, starting to plan for new orders");  //ATAI: HERE REPLAN FOR NEW ORDER CAN BE CALLED
                    } else {
                        if (robotStopped[0])
                            log.info("robot 1 idling");
                        if (robotStopped[1])
                            log.info("robot 2 idling");
                        if (robotStopped[2])
                            log.info("robot 3 idling");
                    }

                } else { //receive a signal to stop dispatching, stopping the robot

                    log.info("stopping robot " + robotId);
                    synchronized (this) {
                        robotStopped[robotId - 1] = true;
                        log.info("SETTING ROBOT STOPPED " + robotId + " TO TRUE INSIDE dispatchEndNode2");
                        if (robotStopped[0] && robotStopped[1] && robotStopped[2])
                            log.info("++++++ ALL ROBOT STOPPED!!!!");
                    }
                }
            }
            if (!ready)
                log.info("+++++++++++ERROR IN ACTION ENDING+++++");
        }
    }


    public void dispatchNode(TemporalNode node) {
        if (node.isValid()) {
            int type = node.getType();
            double time = node.getTime();
            if (type == 1) {
                IntOp action = tgraph.nodeToAction(time, node.getId());
                //ATAI: CHECK THE PRECONDITIONS ON THE KB HERE
                tgraph.checkOrderHardDeadlines(node, getLatestProductionTime());
                log.info("Time: " + time + " Timer: " + getLatestProductionTime() + " - Dispacting action " + action);
                SubProductionTask task = actionMapping.actionToTask(action);
                int robotId = task.getRobotId();
                executing[robotId - 1] = node;
                if (GamePhase.POST_GAME.equals(gameStateDao.getGamePhase())) {
                    log.info("Game is finished, not dispensing new task!");
                    return;
                }
                assignTaskToRobot(task, robotId);
                //ATAI: UPDATE THE EFFECTS ON THE KB HERE

            } else if (type == 2) { //end action
                log.error("error");
            }
        }
    }

    private long getLatestProductionTime() {
        return refboxClient.getLatestGameTimeInSeconds();
    }
}
