package com.grips.scheduler.asp;

import com.grips.config.ProductionConfig;
import com.grips.persistence.dao.*;
import com.grips.persistence.domain.Atom;
import com.grips.persistence.domain.WorldKnowledge;
import com.rcll.domain.MachineClientUtils;
import com.rcll.domain.Order;
import com.rcll.planning.encoding.IntOp;
import com.rcll.planning.encoding.RcllCodedProblem;
import com.rcll.planning.util.TempAtom;
import com.rcll.refbox.RefboxClient;
import fr.uga.pddl4j.util.IntExp;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

@CommonsLog
@Service
public class KnowledgeBase {

    private final AtomDao atomDao;
    @Value("${gameconfig.planningparameters.numberofrobots}")
    private int numberRobots;
    private final MachineInfoRefBoxDao machineInfoRefBoxDao;
    private RcllCodedProblem cp;

    private Map<String, Double> distanceMatrix;


    @Autowired
    private ProductionConfig productionConfig;

    @Value("${gameconfig.productiontimes.normalTask}")
    private int normalTaskDuration;
    @Value("${gameconfig.productiontimes.bufferTask}")
    private int bufferTaskDuration;
    @Value("${gameconfig.productiontimes.moveTask}")
    private int moveTaskDuration;

    private final RefboxClient refboxClient;
    public  KnowledgeBase(AtomDao atomDao, MachineInfoRefBoxDao machineInfoRefBoxDao, ProductionConfig productionConfig,
                          RefboxClient refboxClient) {
        this.atomDao = atomDao;
        this.machineInfoRefBoxDao = machineInfoRefBoxDao;
        this.productionConfig = productionConfig;
        this.refboxClient = refboxClient;
    }


    public void generateInitialKB () {
        //generation of standard atom (robots, stations,  ecc...):
        this.add(new Atom("at", "r1;start"));
        this.add(new Atom("at", "r2;start"));
        this.add(new Atom("free", "r1"));
        this.add(new Atom("free", "r2"));
        this.add(new Atom("n_holding", "r1"));
        this.add(new Atom("n_holding", "r2"));
        this.add(new Atom("empty", "bs"));
        this.add(new Atom("empty", "cs1_input"));
        this.add(new Atom("empty", "cs1_output"));
        this.add(new Atom("empty", "cs1_shelf"));
        this.add(new Atom("empty", "cs2_input"));
        this.add(new Atom("empty", "cs2_output"));
        this.add(new Atom("empty", "cs2_shelf"));
        this.add(new Atom("empty", "ds"));
        this.add(new Atom("empty", "rs1_input"));
        this.add(new Atom("empty", "rs1_output"));
        this.add(new Atom("empty", "rs2_input"));
        this.add(new Atom("empty", "rs2_output"));
        this.add(new Atom("n_station_holding", "cs1"));
        this.add(new Atom("n_station_holding", "cs2"));
        this.add(new Atom("n_station_holding", "rs1"));
        this.add(new Atom("n_station_holding", "rs2"));
        this.add(new Atom("n_cs_station_has_capbase", "cs1"));
        this.add(new Atom("n_cs_station_has_capbase", "cs2"));
        this.add(new Atom("parent_cs", "cs1_input;cs1"));
        this.add(new Atom("parent_cs", "cs1_output;cs1"));
        this.add(new Atom("parent_cs", "cs1_shelf;cs1"));
        this.add(new Atom("parent_cs", "cs2_input;cs2"));
        this.add(new Atom("parent_cs", "cs2_output;cs2"));
        this.add(new Atom("parent_cs", "cs2_shelf;cs2"));
        this.add(new Atom("parent_rs", "rs1_input;rs1"));
        this.add(new Atom("parent_rs", "rs1_output;rs1"));
        this.add(new Atom("parent_rs", "rs2_input;rs2"));
        this.add(new Atom("parent_rs", "rs2_output;rs2"));

        this.add(new Atom("stationcapcolor", productionConfig.getGrey_cap_machine().toLowerCase(Locale.ROOT) + ";cap_grey"));
        this.add(new Atom("stationcapcolor", productionConfig.getBlack_cap_machine().toLowerCase(Locale.ROOT) + ";cap_black"));
        this.add(new Atom("stationringcolor", "rs1;" + ringToColorInPddl((machineInfoRefBoxDao.findByName("C-RS1").getRing1()))));
        this.add(new Atom("stationringcolor", "rs1;" + ringToColorInPddl((machineInfoRefBoxDao.findByName("C-RS1").getRing2()))));
        this.add(new Atom("stationringcolor", "rs2;" + ringToColorInPddl((machineInfoRefBoxDao.findByName("C-RS2").getRing1()))));
        this.add(new Atom("stationringcolor", "rs2;" + ringToColorInPddl((machineInfoRefBoxDao.findByName("C-RS2").getRing2()))));
        this.add(new Atom("requiredbases", "ring_yellow;" + intToString(refboxClient.getRingByColor(MachineClientUtils.RingColor.Yellow).getRawMaterial())));
        this.add(new Atom("requiredbases", "ring_orange;" + intToString(refboxClient.getRingByColor(MachineClientUtils.RingColor.Orange).getRawMaterial())));
        this.add(new Atom("requiredbases", "ring_blue;" + intToString(refboxClient.getRingByColor(MachineClientUtils.RingColor.Blue).getRawMaterial())));
        this.add(new Atom("requiredbases", "ring_green;" + intToString(refboxClient.getRingByColor(MachineClientUtils.RingColor.Green).getRawMaterial())));

        this.add(new Atom("n_basetodispose", machineInfoRefBoxDao.getCapStations().get(0).toString()));
        this.add(new Atom("n_basetodispose", machineInfoRefBoxDao.getCapStations().get(1).toString()));
        this.add(new Atom("loadedbases", "rs1;" + machineInfoRefBoxDao.getBasesLoaded("C-RS1")));
        this.add(new Atom("loadedbases", "rs2;" + machineInfoRefBoxDao.getBasesLoaded("C-RS2")));

        this.add(new Atom("increasebyone", "0;1"));
        this.add(new Atom("increasebyone", "1;2"));
        this.add(new Atom("increasebyone", "2;3"));
        this.add(new Atom("subtract", "0;0;0"));
        this.add(new Atom("subtract", "1;0;1"));
        this.add(new Atom("subtract", "2;0;2"));
        this.add(new Atom("subtract", "3;0;3"));
        this.add(new Atom("subtract", "1;1;0"));
        this.add(new Atom("subtract", "2;1;1"));
        this.add(new Atom("subtract", "3;1;2"));
        this.add(new Atom("subtract", "2;2;0"));
        this.add(new Atom("subtract", "3;2;1"));
        this.add(new Atom("subtract", "3;3;0"));

        this.add(new Atom("distance", "start;rs1_input", distanceMatrix.get("startrs1_input")));
        this.add(new Atom("distance", "start;rs2_input", distanceMatrix.get("startrs2_input")));
        this.add(new Atom("distance", "start;rs1_output", distanceMatrix.get("startrs1_output")));
        this.add(new Atom("distance", "start;rs2_output", distanceMatrix.get("startrs2_output")));
        this.add(new Atom("distance", "start;cs1_input", distanceMatrix.get("startcs1_input")));
        this.add(new Atom("distance", "start;cs2_input", distanceMatrix.get("startcs2_input")));
        this.add(new Atom("distance", "start;cs1_output", distanceMatrix.get("startcs1_output")));
        this.add(new Atom("distance", "start;cs2_output", distanceMatrix.get("startcs2_output")));
        this.add(new Atom("distance", "start;ds", distanceMatrix.get("startds")));
        this.add(new Atom("distance", "start;bs", distanceMatrix.get("startbs")));

        this.add(new Atom("distance", "rs1_input;start", distanceMatrix.get("rs1_inputstart")));
        this.add(new Atom("distance", "rs1_input;rs2_input", distanceMatrix.get("rs1_inputrs2_input")));
        this.add(new Atom("distance", "rs1_input;rs1_output", distanceMatrix.get("rs1_inputrs1_output")));
        this.add(new Atom("distance", "rs1_input;rs2_output", distanceMatrix.get("rs1_inputrs2_output")));
        this.add(new Atom("distance", "rs1_input;cs1_input", distanceMatrix.get("rs1_inputcs1_input")));
        this.add(new Atom("distance", "rs1_input;cs2_input", distanceMatrix.get("rs1_inputcs2_input")));
        this.add(new Atom("distance", "rs1_input;cs1_output", distanceMatrix.get("rs1_inputcs1_output")));
        this.add(new Atom("distance", "rs1_input;cs2_output", distanceMatrix.get("rs1_inputcs2_output")));
        this.add(new Atom("distance", "rs1_input;ds", distanceMatrix.get("rs1_inputds")));
        this.add(new Atom("distance", "rs1_input;bs", distanceMatrix.get("rs1_inputbs")));

        this.add(new Atom("distance", "rs2_input;rs1_input", distanceMatrix.get("rs2_inputrs1_input")));
        this.add(new Atom("distance", "rs2_input;start", distanceMatrix.get("rs2_inputstart")));
        this.add(new Atom("distance", "rs2_input;rs1_output", distanceMatrix.get("rs2_inputrs1_output")));
        this.add(new Atom("distance", "rs2_input;rs2_output", distanceMatrix.get("rs2_inputrs2_output")));
        this.add(new Atom("distance", "rs2_input;cs1_input", distanceMatrix.get("rs2_inputcs1_input")));
        this.add(new Atom("distance", "rs2_input;cs2_input", distanceMatrix.get("rs2_inputcs2_input")));
        this.add(new Atom("distance", "rs2_input;cs1_output", distanceMatrix.get("rs2_inputcs1_output")));
        this.add(new Atom("distance", "rs2_input;cs2_output", distanceMatrix.get("rs2_inputcs2_output")));
        this.add(new Atom("distance", "rs2_input;ds", distanceMatrix.get("rs2_inputds")));
        this.add(new Atom("distance", "rs2_input;bs", distanceMatrix.get("rs2_inputbs")));

        this.add(new Atom("distance", "rs1_output;rs1_input", distanceMatrix.get("rs1_outputrs1_input")));
        this.add(new Atom("distance", "rs1_output;rs2_input", distanceMatrix.get("rs1_outputrs2_input")));
        this.add(new Atom("distance", "rs1_output;start", distanceMatrix.get("rs1_outputstart")));
        this.add(new Atom("distance", "rs1_output;rs2_output", distanceMatrix.get("rs1_outputrs2_output")));
        this.add(new Atom("distance", "rs1_output;cs1_input", distanceMatrix.get("rs1_outputcs1_input")));
        this.add(new Atom("distance", "rs1_output;cs2_input", distanceMatrix.get("rs1_outputcs2_input")));
        this.add(new Atom("distance", "rs1_output;cs1_output", distanceMatrix.get("rs1_outputcs1_output")));
        this.add(new Atom("distance", "rs1_output;cs2_output", distanceMatrix.get("rs1_outputcs2_output")));
        this.add(new Atom("distance", "rs1_output;ds", distanceMatrix.get("rs1_outputds")));
        this.add(new Atom("distance", "rs1_output;bs", distanceMatrix.get("rs1_outputbs")));

        this.add(new Atom("distance", "rs2_output;rs1_input", distanceMatrix.get("rs2_outputrs1_input")));
        this.add(new Atom("distance", "rs2_output;rs2_input", distanceMatrix.get("rs2_outputrs2_input")));
        this.add(new Atom("distance", "rs2_output;rs1_output", distanceMatrix.get("rs2_outputrs1_output")));
        this.add(new Atom("distance", "rs2_output;start", distanceMatrix.get("rs2_outputstart")));
        this.add(new Atom("distance", "rs2_output;cs1_input", distanceMatrix.get("rs2_outputcs1_input")));
        this.add(new Atom("distance", "rs2_output;cs2_input", distanceMatrix.get("rs2_outputcs2_input")));
        this.add(new Atom("distance", "rs2_output;cs1_output", distanceMatrix.get("rs2_outputcs1_output")));
        this.add(new Atom("distance", "rs2_output;cs2_output", distanceMatrix.get("rs2_outputcs2_output")));
        this.add(new Atom("distance", "rs2_output;ds", distanceMatrix.get("rs2_outputds")));
        this.add(new Atom("distance", "rs2_output;bs", distanceMatrix.get("rs2_outputbs")));

        this.add(new Atom("distance", "cs1_input;rs1_input", distanceMatrix.get("cs1_inputrs1_input")));
        this.add(new Atom("distance", "cs1_input;rs2_input", distanceMatrix.get("cs1_inputrs2_input")));
        this.add(new Atom("distance", "cs1_input;rs1_output", distanceMatrix.get("cs1_inputrs1_output")));
        this.add(new Atom("distance", "cs1_input;rs2_output", distanceMatrix.get("cs1_inputrs2_output")));
        this.add(new Atom("distance", "cs1_input;start", distanceMatrix.get("cs1_inputstart")));
        this.add(new Atom("distance", "cs1_input;cs2_input", distanceMatrix.get("cs1_inputcs2_input")));
        this.add(new Atom("distance", "cs1_input;cs1_output", distanceMatrix.get("cs1_inputcs1_output")));
        this.add(new Atom("distance", "cs1_input;cs2_output", distanceMatrix.get("cs1_inputcs2_output")));
        this.add(new Atom("distance", "cs1_input;ds", distanceMatrix.get("cs1_inputds")));
        this.add(new Atom("distance", "cs1_input;bs", distanceMatrix.get("cs1_inputbs")));

        this.add(new Atom("distance", "cs2_input;rs1_input", distanceMatrix.get("cs2_inputrs1_input")));
        this.add(new Atom("distance", "cs2_input;rs2_input", distanceMatrix.get("cs2_inputrs2_input")));
        this.add(new Atom("distance", "cs2_input;rs1_output", distanceMatrix.get("cs2_inputrs1_output")));
        this.add(new Atom("distance", "cs2_input;rs2_output", distanceMatrix.get("cs2_inputrs2_output")));
        this.add(new Atom("distance", "cs2_input;cs1_input", distanceMatrix.get("cs2_inputcs1_input")));
        this.add(new Atom("distance", "cs2_input;start", distanceMatrix.get("cs2_inputstart")));
        this.add(new Atom("distance", "cs2_input;cs1_output", distanceMatrix.get("cs2_inputcs1_output")));
        this.add(new Atom("distance", "cs2_input;cs2_output", distanceMatrix.get("cs2_inputcs2_output")));
        this.add(new Atom("distance", "cs2_input;ds", distanceMatrix.get("cs2_inputds")));
        this.add(new Atom("distance", "cs2_input;bs", distanceMatrix.get("cs2_inputbs")));

        this.add(new Atom("distance", "cs1_output;rs1_input", distanceMatrix.get("cs1_outputrs1_input")));
        this.add(new Atom("distance", "cs1_output;rs2_input", distanceMatrix.get("cs1_outputrs2_input")));
        this.add(new Atom("distance", "cs1_output;rs1_output", distanceMatrix.get("cs1_outputrs1_output")));
        this.add(new Atom("distance", "cs1_output;rs2_output", distanceMatrix.get("cs1_outputrs2_output")));
        this.add(new Atom("distance", "cs1_output;cs1_input", distanceMatrix.get("cs1_outputcs1_input")));
        this.add(new Atom("distance", "cs1_output;cs2_input", distanceMatrix.get("cs1_outputcs2_input")));
        this.add(new Atom("distance", "cs1_output;start", distanceMatrix.get("cs1_outputstart")));
        this.add(new Atom("distance", "cs1_output;cs2_output", distanceMatrix.get("cs1_outputcs2_output")));
        this.add(new Atom("distance", "cs1_output;ds", distanceMatrix.get("cs1_outputds")));
        this.add(new Atom("distance", "cs1_output;bs", distanceMatrix.get("cs1_outputbs")));

        this.add(new Atom("distance", "cs2_output;rs1_input", distanceMatrix.get("cs2_outputrs1_input")));
        this.add(new Atom("distance", "cs2_output;rs2_input", distanceMatrix.get("cs2_outputrs2_input")));
        this.add(new Atom("distance", "cs2_output;rs1_output", distanceMatrix.get("cs2_outputrs1_output")));
        this.add(new Atom("distance", "cs2_output;rs2_output", distanceMatrix.get("cs2_outputrs2_output")));
        this.add(new Atom("distance", "cs2_output;cs1_input", distanceMatrix.get("cs2_outputcs1_input")));
        this.add(new Atom("distance", "cs2_output;cs2_input", distanceMatrix.get("cs2_outputcs2_input")));
        this.add(new Atom("distance", "cs2_output;cs1_output", distanceMatrix.get("cs2_outputcs1_output")));
        this.add(new Atom("distance", "cs2_output;start", distanceMatrix.get("cs2_outputstart")));
        this.add(new Atom("distance", "cs2_output;ds", distanceMatrix.get("cs2_outputds")));
        this.add(new Atom("distance", "cs2_output;bs", distanceMatrix.get("cs2_outputbs")));

        this.add(new Atom("distance", "ds;rs1_input", distanceMatrix.get("dsrs1_input")));
        this.add(new Atom("distance", "ds;rs2_input", distanceMatrix.get("dsrs2_input")));
        this.add(new Atom("distance", "ds;rs1_output", distanceMatrix.get("dsrs1_output")));
        this.add(new Atom("distance", "ds;rs2_output", distanceMatrix.get("dsrs2_output")));
        this.add(new Atom("distance", "ds;cs1_input", distanceMatrix.get("dscs1_input")));
        this.add(new Atom("distance", "ds;cs2_input", distanceMatrix.get("dscs2_input")));
        this.add(new Atom("distance", "ds;cs1_output", distanceMatrix.get("dscs1_output")));
        this.add(new Atom("distance", "ds;cs2_output", distanceMatrix.get("dscs2_output")));
        this.add(new Atom("distance", "ds;start", distanceMatrix.get("dsstart")));
        this.add(new Atom("distance", "ds;bs", distanceMatrix.get("dsbs")));

        this.add(new Atom("distance", "bs;rs1_input", distanceMatrix.get("bsrs1_input")));
        this.add(new Atom("distance", "bs;rs2_input", distanceMatrix.get("bsrs2_input")));
        this.add(new Atom("distance", "bs;rs1_output", distanceMatrix.get("bsrs1_output")));
        this.add(new Atom("distance", "bs;rs2_output", distanceMatrix.get("bsrs2_output")));
        this.add(new Atom("distance", "bs;cs1_input", distanceMatrix.get("bscs1_input")));
        this.add(new Atom("distance", "bs;cs2_input", distanceMatrix.get("bscs2_input")));
        this.add(new Atom("distance", "bs;cs1_output", distanceMatrix.get("bscs1_output")));
        this.add(new Atom("distance", "bs;cs2_output", distanceMatrix.get("bscs2_output")));
        this.add(new Atom("distance", "bs;ds", distanceMatrix.get("bsds")));
        this.add(new Atom("distance", "bs;start", distanceMatrix.get("bsstart")));
    }

    private String ringToColorInPddl(MachineClientUtils.RingColor ringColor) {
        switch (ringColor) {
            case Blue:
                return "ring_blue";
            case Green:
                return "ring_green";
            case Orange:
                return "ring_orange";
            case Yellow:
                return "ring_yellow";
        }
        throw new IllegalArgumentException("Unkown ring color: " + ringColor);
    }


    public void generateOrderKb (List<Order> goals) {
        for (Order p : goals) {
            long id = p.getId();
            this.add(new Atom("stage_c0_0", "p" + id));
            this.add(new Atom("complexity3", "p" + id));
            this.add(new Atom("capcolor", "p" + id + ";cap_grey"));
            this.add(new Atom("ring1color", "p" + id + ";ring_blue"));
            this.add(new Atom("ring2color", "p" + id + ";ring_green"));
            this.add(new Atom("ring3color", "p" + id + ";ring_orange"));
            this.add(new Atom("tocheck", "p" + id));
        }
    }


    public void printKB () {
        for( Atom at : atomDao.findAll())
            System.out.println(at.toString());
    }


    public void prettyPrintKBtoProblemFile (FileWriter fr) throws IOException {
        for (Atom at : atomDao.findAll())
            if(!at.getName().equals("holdafter") )
                fr.write(at.prettyPrinter());
    }


    public void add (Atom at) {
        if (atomDao.findByNameAndAttributes(at.getName(), at.getAttributes()).size() == 0 )
            atomDao.save(at);
        else
            log.warn("Trying to add to KB atom " + at.toString() + " already present");

    }



    public boolean isNotInKB (String pd, String attributes) {
        return atomDao.findByNameAndAttributes(pd, attributes).isEmpty();
    }

    public void setCp (RcllCodedProblem cp) {
        this.cp = cp;
    }


    public String intToString (int number) {
        switch (number) {

            case 0:
                return "zero";

            case 1:
                return "one";

            case 2:
                return "two";

            case 3:
                return "three";

            default:
                return null;

        }
    }


    public void purgeKB () {
        atomDao.deleteAll();
    }



    public void setTemporaryKBasMain(List<TempAtom> tempKB) {
        this.purgeKB();
        for (TempAtom tat : tempKB) {
            if (tat.isPredicate())
                this.add(new Atom(tat.getName(), tat.getAttributes()));
            else
                this.add(new Atom(tat.getName(), tat.getAttributes(), tat.getResult()));
        }
    }





    /**
     * check if end precondition of action are satisfied
     * @param action
     * @return
     */



}
