package com.grips.scheduler.asp.dispatcher;


import com.grips.config.RefboxConfig;
import com.grips.persistence.domain.ProductOrder;
import com.grips.persistence.domain.SubProductionTask;
import com.grips.persistence.domain.SubProductionTaskBuilder;
import com.grips.persistence.misc.GripsDataService;
import com.rcll.domain.Cap;
import com.rcll.domain.MachineClientUtils;
import com.rcll.domain.TeamColor;
import com.rcll.planning.encoding.IntOp;
import com.shared.domain.BaseColor;
import com.shared.domain.MachineSide;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.MachineDescriptionProtos;
import org.robocup_logistics.llsf_msgs.ProductColorProtos;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

@CommonsLog
public class ActionMapping {

    private List<String> cpConstants;
    private final GripsDataService<ProductOrder, Long> productOrderDao;

    private String colorprefix;

    public ActionMapping(GripsDataService<ProductOrder, Long> productOrderDao, RefboxConfig refboxConfig) {
        this.productOrderDao = productOrderDao;

        try {
            colorprefix = TeamColor.valueOf(refboxConfig.getTeamcolor()).equals(TeamColor.CYAN) ? "C-" : "M-";
        } catch (Exception e) {
            System.out.println("Team color undefined! Setting default to CYAN!");
            colorprefix = "C-";
        }

    }

    public void updateCodedProblem(List<String> cpConstants) {
        this.cpConstants = cpConstants;
    }

    public SubProductionTask actionToTask (IntOp action) {
        String name = action.getName();
        SubProductionTask task;
        int orderIdIndex;
        int orderId;
        int robotIndex =  action.getInstantiations()[0];
        int robotId = Character.getNumericValue(cpConstants.get(robotIndex).charAt(1));
        switch (name) {

            //ATAI parameters that have to be set for specific kind of tasks:
            //.setOrderInfoId((long) orderId) in all tasks in which you are manipulating a subpiece of the product (buffering cs, retrieving the base from the CS after the buffering, retrieving a base for a RS and delivering a base to the RS slide are excluded)
            //.setIsDemandTask(true) for all the tasks excluded by .setOrderInfoId (buffering cs, retrieving the base from the CS after the buffering, retrieving a base for a RS and delivering a base to the RS slide)
            //.setRequiredColor for retrieving a base, delivering partial product to RS to mount a ring, delivering partial product to CS to mount a cap
            //.setOptCode("RETRIEVE_CAP") for the buffer cap action
            //.setOptCode(MachineDescriptionProtos.CSOp.MOUNT_CAP.toString()) when delivering a partial product to a CS to mount a cap
            //.setOptCode(order.getDeliveryGate()+"") when delivering the final product to the DS (order is a ProductOrder object stored in the ProductOrderDAO)

            case "getBaseFromBScriticalTask":
                orderIdIndex =  action.getInstantiations()[1];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(1));
                Optional<ProductOrder> optorder = productOrderDao.findById((long) orderId);
                optorder.get();
                ProductOrder order = optorder.get();
                BaseColor baseColor = order.getBaseColor();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("getBaseFromBSCT")
                        .setMachine(colorprefix + "BS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.GET)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.OUTPUT)                    //ATAI: side can be INPUT, OUTPUT, SHELF or SLIDE
                        .setOrderInfoId((long) orderId)                 //ATAI: this field is not required for "resource" tasks (like buffering the cap or retrieving a base to be delivered to the slide of a RS)
                        .setRequiredColor(baseColor.toString())
                        .setOptCode(null)
                        .build();
                break;

            case "deliverProductC0ToCScriticalTask":
                orderIdIndex =  action.getInstantiations()[2];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(2));
                Optional<ProductOrder> optorderC0 = productOrderDao.findById((long) orderId);
                optorderC0.get();
                ProductOrder orderC0 = optorderC0.get();
                Cap capColor = orderC0.getCapColor();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("deliverProductC0ToCT")
                        .setMachine(colorprefix + "CS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.INPUT)                    //ATAI: side can be INPUT, OUTPUT, SHELF or SLIDE
                        .setOrderInfoId((long) orderId)                 //ATAI: this field is not required for "resource" tasks (like buffering the cap or retrieving a base to be delivered to the slide of a RS)
                        .setRequiredColor(capColor.toString())
                        .setOptCode(MachineDescriptionProtos.CSOp.MOUNT_CAP.toString())
                        .build();
                break;

            case "deliverProductC1ToCScriticalTask":
                orderIdIndex =  action.getInstantiations()[3];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(3));
                Optional<ProductOrder> optorderC1 = productOrderDao.findById((long) orderId);
                optorderC1.get();
                ProductOrder orderC1 = optorderC1.get();
                Cap capColor2 = orderC1.getCapColor();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("deliverProductC1ToCSCT")
                        .setMachine(colorprefix + "CS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.INPUT)                    //ATAI: side can be INPUT, OUTPUT, SHELF or SLIDE
                        .setOrderInfoId((long) orderId)                 //ATAI: this field is not required for "resource" tasks (like buffering the cap or retrieving a base to be delivered to the slide of a RS)
                        .setRequiredColor(capColor2.toString())
                        .setOptCode(MachineDescriptionProtos.CSOp.MOUNT_CAP.toString())
                        .build();
                break;

            case "deliverProductC2ToCScriticalTask":
                orderIdIndex =  action.getInstantiations()[4];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(4));
                Optional<ProductOrder> optorderC2 = productOrderDao.findById((long) orderId);
                optorderC2.get();
                ProductOrder orderC2 = optorderC2.get();
                Cap capColor3 = orderC2.getCapColor();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("deliverProductC2ToCSCT")
                        .setMachine(colorprefix + "CS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.INPUT)                    //ATAI: side can be INPUT, OUTPUT, SHELF or SLIDE
                        .setOrderInfoId((long) orderId)                 //ATAI: this field is not required for "resource" tasks (like buffering the cap or retrieving a base to be delivered to the slide of a RS)
                        .setRequiredColor(capColor3.toString())
                        .setOptCode(MachineDescriptionProtos.CSOp.MOUNT_CAP.toString())
                        .build();
                break;

            case "deliverProductC3ToCScriticalTask":
                orderIdIndex =  action.getInstantiations()[5];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(5));
                Optional<ProductOrder> optorderC3 = productOrderDao.findById((long) orderId);
                optorderC3.get();
                ProductOrder orderC3 = optorderC3.get();
                Cap capColor4 = orderC3.getCapColor();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("deliverProductC3ToCSCT")
                        .setMachine(colorprefix + "CS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.INPUT)                    //ATAI: side can be INPUT, OUTPUT, SHELF or SLIDE
                        .setOrderInfoId((long) orderId)                 //ATAI: this field is not required for "resource" tasks (like buffering the cap or retrieving a base to be delivered to the slide of a RS)
                        .setRequiredColor(capColor4.toString())
                        .setOptCode(MachineDescriptionProtos.CSOp.MOUNT_CAP.toString())
                        .build();
                break;

            case "getCapBaseFromCSresourceTask":
                orderIdIndex =  action.getInstantiations()[6];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(6));
                Optional<ProductOrder> optorderCap = productOrderDao.findById((long) orderId);
                optorderCap.get();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("getCapBaseFromCSRT")
                        .setMachine(colorprefix + "CS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.GET)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.SHELF)                    //ATAI: side can be INPUT, OUTPUT, SHELF or SLIDE
                        .setIsDemandTask(true)                 //ATAI: this field is not required for "resource" tasks (like buffering the cap or retrieving a base to be delivered to the slide of a RS)
                        .setOptCode("RETRIEVE_CAP")
                        .build();
                break;

            case "getProductFromCScriticalTask":
                orderIdIndex =  action.getInstantiations()[7];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(7));
                Optional<ProductOrder> optorderProduct = productOrderDao.findById((long) orderId);
                optorderProduct.get();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("getProductFromCSCT")
                        .setMachine(colorprefix + "CS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.GET)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.OUTPUT)                    //ATAI: side can be INPUT, OUTPUT, SHELF or SLIDE
                        .setOrderInfoId((long) orderId)                 //ATAI: this field is not required for "resource" tasks (like buffering the cap or retrieving a base to be delivered to the slide of a RS)
                        .setOptCode(null)
                        .build();
                break;

            case "deliverProductToRS1criticalTask":
                orderIdIndex =  action.getInstantiations()[8];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(8));
                Optional<ProductOrder> optorderrs1 = productOrderDao.findById((long) orderId);
                optorderrs1.get();
                ProductOrder orderrs1 = optorderrs1.get();
                MachineClientUtils.RingColor ringColor = orderrs1.getRing1();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("deliverProductToRS1CT")
                        .setMachine(colorprefix + "RS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.INPUT)                    //ATAI: side can be INPUT, OUTPUT, SHELF or SLIDE
                        .setIsDemandTask(true)
                        .setRequiredColor(ringColor.toString())
                        .setOptCode(null)
                        .build();
                break;

            case "deliverProductToRS2criticalTask":
                orderIdIndex =  action.getInstantiations()[9];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(9));
                Optional<ProductOrder> optorderrs2 = productOrderDao.findById((long) orderId);
                optorderrs2.get();
                ProductOrder orderrs2 = optorderrs2.get();
                MachineClientUtils.RingColor ringColor2 = orderrs2.getRing2();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("deliverProductToRS2CT")
                        .setMachine(colorprefix + "RS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.INPUT)                    //ATAI: side can be INPUT, OUTPUT, SHELF or SLIDE
                        .setIsDemandTask(true)
                        .setRequiredColor(ringColor2.toString())
                        .setOptCode(null)
                        .build();
                break;

            case "deliverProductToRS3criticalTask":
                orderIdIndex =  action.getInstantiations()[10];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(10));
                Optional<ProductOrder> optorderrs3 = productOrderDao.findById((long) orderId);
                optorderrs3.get();
                ProductOrder orderrs3 = optorderrs3.get();
                MachineClientUtils.RingColor ringColor3 = orderrs3.getRing3();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("deliverProductToRS3CT")
                        .setMachine(colorprefix + "RS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.INPUT)                    //ATAI: side can be INPUT, OUTPUT, SHELF or SLIDE
                        .setIsDemandTask(true)
                        .setRequiredColor(ringColor3.toString())
                        .setOptCode(null)
                        .build();
                break;

            case "getProductFromRSCriticalTask":
                orderIdIndex =  action.getInstantiations()[11];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(11));
                Optional<ProductOrder> optorderprs = productOrderDao.findById((long) orderId);
                optorderprs.get();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("getProductFromRSCT")
                        .setMachine(colorprefix + "RS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.GET)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.OUTPUT)                    //ATAI: side can be INPUT, OUTPUT, SHELF or SLIDE
                        .setOrderInfoId((long) orderId)                 //ATAI: this field is not required for "resource" tasks (like buffering the cap or retrieving a base to be delivered to the slide of a RS)
                        .setOptCode(null)
                        .build();
                break;

            case "getBaseFromBSResourceTask": //not final product
                orderIdIndex =  action.getInstantiations()[12];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(12));
                Optional<ProductOrder> optorderbs = productOrderDao.findById((long) orderId);
                optorderbs.get();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("getBaseFromBSRT")
                        .setMachine(colorprefix + "BS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.GET)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.OUTPUT)                    //ATAI: side can be INPUT, OUTPUT, SHELF or SLIDE
                        .setIsDemandTask(true)                 //ATAI: this field is not required for "resource" tasks (like buffering the cap or retrieving a base to be delivered to the slide of a RS)
                        .setOptCode(null)
                        .build();
                break;

            case "getBaseFromCSResourceTask": //not final product
                orderIdIndex =  action.getInstantiations()[13];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(13));
                Optional<ProductOrder> optordercs = productOrderDao.findById((long) orderId);
                optordercs.get();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("getBaseFromCSRT")
                        .setMachine(colorprefix + "CS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.GET)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.OUTPUT)                    //ATAI: side can be INPUT, OUTPUT, SHELF or SLIDE
                        .setIsDemandTask(true)                //ATAI: this field is not required for "resource" tasks (like buffering the cap or retrieving a base to be delivered to the slide of a RS)
                        .setOptCode(null)
                        .build();
                break;

            case "deliverBaseToRSResourceTask": //not final product
                orderIdIndex =  action.getInstantiations()[14];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(14));
                Optional<ProductOrder> optorderrst = productOrderDao.findById((long) orderId);
                optorderrst.get();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("deliverBaseToRSRT")
                        .setMachine(colorprefix + "RS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.SLIDE)                    //ATAI: side can be INPUT, OUTPUT, SHELF or SLIDE
                        .setIsDemandTask(true)                //ATAI: this field is not required for "resource" tasks (like buffering the cap or retrieving a base to be delivered to the slide of a RS)
                        .setOptCode(null)
                        .build();
                break;

            case "deliverProductToDScriticalTask": //final product
                orderIdIndex =  action.getInstantiations()[15];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(15));
                Optional<ProductOrder> optorderds = productOrderDao.findById((long) orderId);
                optorderds.get();
                ProductOrder orderds = optorderds.get();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("deliverProductToDSCT")
                        .setMachine(colorprefix + "DS")                 //ATAI: machine type can be BS, RS, CS or DS
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
                        .setSide(MachineSide.INPUT)                    //ATAI: side can be INPUT, OUTPUT, SHELF or SLIDE
                        .setOrderInfoId((long) orderId)                 //ATAI: this field is not required for "resource" tasks (like buffering the cap or retrieving a base to be delivered to the slide of a RS)
                        .setOptCode(orderds.getDeliveryGate()+"")
                        .build();
                break;


            default:
                System.out.println("case not managed");
                task = null;
                break;
        }
        task.setRobotId(robotId);
        return task;
    }
}
