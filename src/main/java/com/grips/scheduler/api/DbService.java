package com.grips.scheduler.api;

import com.grips.config.StationsConfig;
import com.grips.persistence.dao.LockPartDao;
import com.grips.persistence.dao.MachineInfoRefBoxDao;
import com.grips.tools.PathEstimator;
import com.rcll.domain.MachineClientUtils;
import com.rcll.domain.Ring;
import com.rcll.refbox.RefboxClient;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
@CommonsLog
public class DbService {
    private final MachineInfoRefBoxDao machineInfoRefBoxDao;
    private final LockPartDao lockPartDao;
    private final StationsConfig stationsConfig;

    private final RefboxClient refboxClient;
    private final PathEstimator pathEstimator;

    public DbService(MachineInfoRefBoxDao machineInfoRefBoxDao, LockPartDao lockPartDao,
                     StationsConfig stationsConfig, RefboxClient refboxClient, PathEstimator pathEstimator) {
        this.refboxClient = refboxClient;
        this.machineInfoRefBoxDao = machineInfoRefBoxDao;
        this.lockPartDao = lockPartDao;
        this.stationsConfig = stationsConfig;
        this.pathEstimator = pathEstimator;
    }

    public boolean checkProductionReady() {

        try {
            List<Ring> allRings = Arrays.asList(
                    refboxClient.getRingByColor(MachineClientUtils.RingColor.Yellow),
                    refboxClient.getRingByColor(MachineClientUtils.RingColor.Orange),
                    refboxClient.getRingByColor(MachineClientUtils.RingColor.Blue),
                    refboxClient.getRingByColor(MachineClientUtils.RingColor.Green));

            if (stationsConfig.isUseRs1() && stationsConfig.isUseRs2()) {
                for (Ring r : allRings) {
                    if (machineInfoRefBoxDao.countByRing1OrRing2(r.getColor(), r.getColor()) == 0) {
                        log.error("Ring is not in any machine: " + r.getColor());
                        return false;
                    }
                }
            } else {
                //todo add checking for only one RS!! now we can skip that!
            }
            if (pathEstimator.availableMachines() < 6) { //Todo chceck!!
                log.warn("Only: " + pathEstimator.availableMachines() + " machines in path estimator, waiting...");
                return false;
            }
            return true;
        } catch (Exception exp) {
            log.error("Error on checking if ProductionReady: ", exp);
            return false;
        }
    }

    public boolean hasOrders() {
        if (refboxClient.getAllOrders().size() >= 3) {
            return true;
        }
        log.error("Not yet enough Orders!");

        return false;
    }


    public int getMaterialCount(String machine) {
        int returner = lockPartDao.countByMachine(machine);
        log.info("getMaterialCount for Machine: : " + machine + " is:" + returner);
        return returner;
    }
}
