package com.grips.scheduler.production;

import com.grips.config.RefboxConfig;
import com.rcll.domain.TeamColor;
import com.grips.scheduler.api.IScheduler;
import com.grips.scheduler.exploration.ExplorationScheduler;
import org.robocup_logistics.llsf_msgs.MachineInfoProtos;
import org.robocup_logistics.llsf_msgs.TeamProtos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class SubProductionTaskUpdater {
    @Autowired
    @Qualifier("production-scheduler")
    private IScheduler productionScheduler;

    @Autowired
    private ExplorationScheduler explorationScheduler;


    @Autowired
    private RefboxConfig refboxConfig;

    public void updateTaskPerMachine(MachineInfoProtos.MachineInfo info) {
        //todo remove hard coded TEAM value!
        if (info.getTeamColor().equals(TeamProtos.Team.valueOf(refboxConfig.getTeamcolor()))) {
            info.getMachinesList().forEach(machine -> {
                productionScheduler.handleMachineInfo(machine);
                explorationScheduler.handleMachineInfo(machine);
            });
        }
    }
}
