package com.grips.scheduler.challanges;

import com.rcll.domain.Peer;
import com.grips.scheduler.api.IScheduler;
import com.rcll.planning.encoding.RcllCodedProblem;
import com.rcll.planning.encoding.Encoder;
import com.rcll.planning.planner.Planner;
import com.grips.scheduler.challanges.utils.DataSet;
import com.robot_communication.services.PrsTaskCreator;
import com.robot_communication.services.RobotClient;
import fr.uga.pddl4j.parser.Domain;
import fr.uga.pddl4j.parser.Parser;
import fr.uga.pddl4j.parser.Problem;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static com.grips.scheduler.challanges.utils.KMeans.kmeans;

@CommonsLog
public class NavigationScheduler implements IScheduler {
    private final RobotClient robotClient;
    private NavigationChallengeProtos.NavigationRoutes routes;
    private String planner = "./optic";
    private String domainS = "tspdom.pddl";
    private String problemS = "problem.pddl";

    private ArrayList<Integer> zonesR1;
    private ArrayList<Integer> zonesR2;
    private ArrayList<Integer> zonesR3;

    private int indexR1 = 0;
    private int indexR2 = 0;
    private int indexR3 = 0;

    private int waitR1;
    private int waitR2;
    private int waitR3;

    int WAIT = 1;

    public NavigationScheduler(RobotClient robotClient) {
        this.robotClient = robotClient;
        zonesR1 = new ArrayList<>();
        zonesR2 = new ArrayList<>();
        zonesR3 = new ArrayList<>();
        indexR1 = 0;
        indexR2 = 0;
        indexR3 = 0;
        waitR1 = 5;
        waitR2 = 5;
        waitR3 = 5;
    }

    @Override
    public void handleBeaconSignal(BeaconSignalProtos.BeaconSignal beacon_signal, int robotId, Peer robot) {
        if (routes == null) {
            log.warn("No routes received yet!");
            return;
        }
        if (beacon_signal.getTask().getTaskId() == -1) {
            log.info("Robot has no task, sending it to next zone!");
            switch (robotId) {
                case 1:
                    if (waitR1 > WAIT) {
                        if (indexR1 == zonesR1.size()) {
                            break;
                        }
                        moveToTask(robotId, "M_Z" + zonesR1.get(indexR1) + "_waiting");
                        indexR1++;
                        waitR1 = -1;
                    } else {
                        waitR1++;
                    }
                    break;
                case 2:
                    if (waitR2 > WAIT) {
                        if (indexR2 == zonesR2.size()) {
                            break;
                        }
                        moveToTask(robotId, "M_Z" + zonesR2.get(indexR2) + "_waiting");
                        indexR2++;
                        waitR2 = -1;
                    } else {
                        waitR2++;
                    }
                    break;
                case 3:
                    if (waitR3 > WAIT) {
                        if (indexR3 == zonesR3.size()) {
                            break;
                        }
                        moveToTask(robotId, "M_Z" + zonesR3.get(indexR3) + "_waiting");
                        indexR3++;
                        waitR3 = -1;
                    } else {
                        waitR3++;
                    }
                    break;
            }
        } else {
            log.debug("Robot " + robotId + " already has a task!");
        }

    }

    @Override
    public boolean handleRobotTaskResult(AgentTasksProtos.AgentTask prsTask) {
        log.error("NavigationScheduler::handleBeaconSignal not implemented!");
        return false;
    }

    @Override
    public void handleMachineInfo(MachineInfoProtos.Machine machine) {
        log.error("NavigationScheduler::handleBeaconSignal not implemented!");
    }

    public void updateRoutes(NavigationChallengeProtos.NavigationRoutes update) {
        if (this.routes == null) {
            routes = update;
            while (clusterRoutes() == false) {
                log.error("Error generating zones to visi!");
            }
            log.info("Generated zones to visit!");
            String r1 = zonesR1.stream().map(Object::toString).collect(Collectors.joining(" -> ")).toString();
            String r2 = zonesR2.stream().map(Object::toString).collect(Collectors.joining(" -> ")).toString();
            String r3 = zonesR3.stream().map(Object::toString).collect(Collectors.joining(" -> ")).toString();
            log.info("R1: " + r1);
            log.info("R2: " + r2);
            log.info("R3: " + r3);
        }
    }

    public Boolean clusterRoutes() {
        LinkedList<String> names = new LinkedList<String>();
        names.add("X");
        names.add("Y");

        LinkedList<List<Integer>> values = new LinkedList<List<Integer>>();

        for (int i = 0; i < routes.getRoutesList().get(0).getRouteList().size(); ++i) {
            ZoneProtos.Zone zone = routes.getRoutesList().get(0).getRouteList().get(i);
            Integer number = zone.getNumber() - 1000;
            List<Integer> coord = new LinkedList<>();
            coord.add((number - (number % 10)) / 10);
            coord.add(number % 10);
            values.add(coord);
        }

        DataSet data = new DataSet(names, values);

        // remove prior classification attr if it exists (input any irrelevant attributes)
        data.removeAttr("Class");

        // cluster
        for (int i = 0; i < 100; ++i) {
            kmeans(data, 3);
            if (data.validate())
                break;
        }

        // output into a csv
        List<List<Integer>> cluster_0 = data.dataToList(0);
        List<List<Integer>> cluster_1 = data.dataToList(1);
        List<List<Integer>> cluster_2 = data.dataToList(2);

        List<List<Double>> graph_0 = new LinkedList<>();
        for (List<Integer> zone1 : cluster_0) {
            List<Double> distance = new LinkedList<>();
            for (List<Integer> zone2 : cluster_0) {
                distance.add(Math.sqrt(Math.pow(zone2.get(0) - zone1.get(0), 2) + Math.pow(zone2.get(1) - zone1.get(1), 2)));
            }
            graph_0.add(distance);
        }
        //int n = cluster_0.size();
        //boolean[] v = new boolean[n];
        //v[0] = true;
        //Double ans = Double.MAX_VALUE;
        //List<Integer> zones = new LinkedList<>();
        //ans = tsp(graph_0, v, 0, n, 1, 0.0, ans, zones);
        data.createCsvOutput();
        //System.out.println(zones);
        //System.out.println( n);
        ArrayList<int[]> zones_robot_0 = new ArrayList<>();
        ArrayList<int[]> zones_robot_1 = new ArrayList<>();
        ArrayList<int[]> zones_robot_2 = new ArrayList<>();
        zones_robot_0.add(new int[]{5, 1});
        zones_robot_1.add(new int[]{4, 1});
        zones_robot_2.add(new int[]{3, 1});

        for (int i = 0; i < cluster_0.size(); ++i) {
            zones_robot_0.add(new int[]{cluster_0.get(i).get(0), cluster_0.get(i).get(1)});
        }
        for (int i = 0; i < cluster_1.size(); ++i) {
            zones_robot_1.add(new int[]{cluster_1.get(i).get(0), cluster_1.get(i).get(1)});
        }
        for (int i = 0; i < cluster_2.size(); ++i) {
            zones_robot_2.add(new int[]{cluster_2.get(i).get(0), cluster_2.get(i).get(1)});
        }
        ArrayList<Integer> tmpR1 = solveTSP(zones_robot_0);
        ArrayList<Integer> tmpR2 = solveTSP(zones_robot_1);
        ArrayList<Integer> tmpR3 = solveTSP(zones_robot_2);
        if (tmpR1.size() < 3 || tmpR2.size() < 3 || tmpR3.size() < 3) {
            return false;
        }
        tmpR1.forEach(index -> {
            Integer zone = cluster_0.get(index - 2).get(0) * 10 + cluster_0.get(index - 2).get(1);
            zonesR1.add(zone);
        });

        tmpR2.forEach(index -> {
            Integer zone = cluster_1.get(index - 2).get(0) * 10 + cluster_1.get(index - 2).get(1);
            zonesR2.add(zone);
        });

        tmpR3.forEach(index -> {
            Integer zone = cluster_2.get(index - 2).get(0) * 10 + cluster_2.get(index - 2).get(1);
            zonesR3.add(zone);
        });
        return true;
    }

    /*public Double tsp(List<List<Double>> graph, boolean[] v, int currPos, int n, int count, Double cost, Double ans, List<Integer> zones)
    {
        if (count == n && graph.get(currPos).get(0) > 0)
        {
            if(ans != Math.min(ans, cost + graph.get(currPos).get(0)))
                zones.add(currPos);
            ans = Math.min(ans, cost + graph.get(currPos).get(0));
            return ans;
        }

        for (int i = 0; i < n; i++)
        {
            if (v[i] == false && graph.get(currPos).get(i) > 0)
            {

                // Mark as visited
                v[i] = true;
                ans = tsp(graph, v, i, n, count + 1,
                        cost + graph.get(currPos).get(i), ans, zones);

                // Mark ith node as unvisited
                v[i] = false;
            }
        }
        return ans;
    }*/

    public ArrayList<Integer> solveTSP(ArrayList<int[]> zones) {
        //PARSING PHASE

        generateTSPproblemfile(zones);

        final StringBuilder strb = new StringBuilder();
        Parser parser = new Parser();
        try {
            parser.parse(domainS, problemS);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        log.warn("parsed\n");

        //ENCODING PHASE

        //Domain p = parser.getDomain();
        //log.warn(p.toString()+"\n");

        final Domain domain = parser.getDomain();
        final Problem problem = parser.getProblem();

        try {
            RcllCodedProblem cp;
            cp = Encoder.encode(domain, problem);
            //PLANNING PHASE

            Planner pl = new Planner(domainS, problemS, cp);
            ArrayList<Integer> orderToVisit = pl.TSPplan();

            System.out.println("order " + orderToVisit);

            return orderToVisit;
        } catch (IllegalArgumentException ilException) {
            log.error("the problem to encode is not ADL, \":requirements\" not supported at this time\n");
            return null;
        }
    }

    private void generateTSPproblemfile(ArrayList<int[]> zones) {
        try {
            File file = new File(problemS);
            FileWriter fr = new FileWriter(file, false);
            fr.write("(define (problem ten-cities)\n" +
                    "  (:domain tsp-strips)\n" +
                    "  (:objects ");
            for (int i = 1; i <= zones.size(); i++) {
                fr.write("zone" + i + " ");
            }
            fr.write(")\n" +
                    "  (:init ");
            for (int i = 1; i <= zones.size(); i++) {
                for (int j = 1; j <= zones.size(); j++) {
                    if (i != j) {
                        fr.write("(connected zone" + i + " zone" + j + ")\n");
                        double distance = 100 - Math.sqrt(Math.pow(zones.get(i - 1)[0] - zones.get(j - 1)[0], 2) + Math.pow(zones.get(i - 1)[1] - zones.get(j - 1)[1], 2));
                        fr.write("(=(distance zone" + i + " zone" + j + ") " + distance + ")\n");
                    }
                }
            }
            fr.write("(visited zone1)");
            fr.write("(in zone1)");
            for (int i = 2; i <= zones.size(); i++) {
                fr.write("(not-visited zone" + i + ")\n");
            }
            fr.write(")\n" +
                    "  (:goal (and ");
            for (int i = 1; i <= zones.size(); i++) {
                fr.write("(visited zone" + i + ")\n");
            }
            fr.write("))\n" +
                    "    (:metric maximize (total-cost))\n" +
                    "  )");
            fr.close();

        } catch (IOException e) {
            log.error(e);
        }
    }

    public void moveToTask(long robotID, String zone) {
        AgentTasksProtos.AgentTask task = new PrsTaskCreator().createMoveToWaypointTask(robotID, new Random().nextInt(), zone);
        robotClient.sendPrsTaskToRobot(task);
    }
}
