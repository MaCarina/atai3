package com.grips.robot.msg_handler;

import com.grips.config.RefboxConfig;
import com.rcll.domain.Peer;
import com.grips.persistence.dao.BeaconSignalFromRobotDao;
import com.grips.persistence.dao.GameStateDao;
import com.grips.persistence.dao.MachineInfoRefBoxDao;
import com.grips.persistence.domain.BeaconSignalFromRobot;
import com.rcll.domain.PeerState;
import com.rcll.protobuf_lib.RobotConnections;
import com.rcll.protobuf_lib.RobotMessageRegister;
import com.rcll.refbox.RefboxClient;
import com.grips.scheduled_tasks.SendAllKnownMachinesTask;
import com.grips.scheduler.api.DbService;
import com.grips.scheduler.api.IScheduler;
import com.grips.scheduler.challanges.GraspingScheduler;
import com.grips.scheduler.exploration.ExplorationScheduler;
import com.robot_communication.services.RobotClient;
import com.shared.domain.GamePhase;
import lombok.extern.apachecommons.CommonsLog;
import org.dozer.Mapper;
import org.robocup_logistics.llsf_msgs.BeaconSignalProtos;
import org.robocup_logistics.llsf_utils.Key;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import java.net.Socket;
import java.util.function.Consumer;

@CommonsLog
public class BeaconMsgHandler implements Consumer<BeaconSignalProtos.BeaconSignal> {

    @Value("${gameconfig.scheduler.task_delay}")
    private int SLEEP_TASK_ASSIGNMENT;

    private final Socket socket;
    private final RobotConnections robotConnections;
    private final GameStateDao gameStateDao;
    private final Mapper mapper;
    private final BeaconSignalFromRobotDao beaconSignalFromRobotDao;
    private final RefboxClient refboxClient;
    private final RobotClient robotClient;
    private final DbService dbService;
    private final IScheduler productionScheduler;
    private final ExplorationScheduler explorationScheduler;
    private final SendAllKnownMachinesTask sendAllKnownMachines;

    private final MachineInfoRefBoxDao machineInfoRefBoxDao;

    private boolean currentlyInAssignTask = false;

    private final RefboxConfig refboxConfig;

    private final int minOrdersToPlan;
    private final int numberRobots;

    public BeaconMsgHandler(Socket socket,
                            RobotConnections robotConnections,
                            GameStateDao gameStateDao,
                            Mapper mapper,
                            BeaconSignalFromRobotDao beaconSignalFromRobotDao,
                            RefboxClient refboxClient,
                            RobotClient robotClient,
                            DbService dbService,
                            @Qualifier("production-scheduler") IScheduler productionScheduler,
                            ExplorationScheduler explorationScheduler,
                            SendAllKnownMachinesTask sendAllKnownMachines,
                            MachineInfoRefBoxDao machineInfoRefBoxDao,
                            RefboxConfig refboxConfig,
                            int minOrdersToPlan,
                            int numberRobots) {
        this.socket = socket;
        this.robotConnections = robotConnections;
        this.gameStateDao = gameStateDao;
        this.mapper = mapper;
        this.beaconSignalFromRobotDao = beaconSignalFromRobotDao;
        this.refboxClient = refboxClient;
        this.robotClient = robotClient;
        this.dbService = dbService;
        this.productionScheduler = productionScheduler;
        this.explorationScheduler = explorationScheduler;
        this.sendAllKnownMachines = sendAllKnownMachines;
        this.machineInfoRefBoxDao = machineInfoRefBoxDao;
        this.refboxConfig = refboxConfig;
        this.minOrdersToPlan = minOrdersToPlan;
        this.numberRobots = numberRobots;
    }

    @Override
    public void accept(BeaconSignalProtos.BeaconSignal beacon_signal) {

        int robotId = beacon_signal.getNumber();

        if (!robotConnections.isRobotConnected(beacon_signal.getNumber())) {
            Peer robot = mapper.map(beacon_signal, Peer.class);
            robot.setLastActive(System.currentTimeMillis());
            //robot.setId(beacon_signal.getBeaconSignal().getNumber());
            robot.setConnection(socket);
            robot.setWaiting(false);
            robotConnections.addRobot(robot);

            sendAllKnownMachines.sendAllKnownMachines((long) robotId);
        } else {
            robotConnections.getRobot(beacon_signal.getNumber()).setLastActive(System.currentTimeMillis());
        }


        acceptPrivate(beacon_signal);
    }

    private void acceptPrivate(BeaconSignalProtos.BeaconSignal beacon_signal) {
        BeaconSignalFromRobot bSigRobot = mapper.map(beacon_signal, BeaconSignalFromRobot.class);
        bSigRobot.setTaskId(beacon_signal.getTask().getTaskId());
        Key key = RobotMessageRegister.getInstance().get_msg_key_from_class(BeaconSignalProtos.BeaconSignal.class);

        int robotId = 0;
        try {
            robotId = Integer.parseInt(bSigRobot.getRobotId());
        } catch (Exception e) {
            log.error("Robot ID cannot be parsed!");
            return;
        }
        if (refboxClient.getTeamColor().isEmpty()) {
            log.warn("TeamColor not yet configured");
            return;
        }

        bSigRobot.setLocalTimestamp(System.currentTimeMillis());
        beaconSignalFromRobotDao.save(bSigRobot);

        Peer robot = robotConnections.getRobot(robotId);
        if (robot == null) {
            log.error("Robot with ID " + robotId + " not found in connection manager!!!");
            return;
        }

        refboxClient.sendBeaconSignal(robotId,
                beacon_signal.getPeerName(),
                beacon_signal.getPose().getX(),
                beacon_signal.getPose().getY(),
                beacon_signal.getPose().getOri());

        if (!currentlyInAssignTask && PeerState.ACTIVE.equals(robot.getRobotState())) {
            currentlyInAssignTask = true;
            // if a robot sends a beacon signal in exploration, we assign him a task
            // if the robot has no task
            if (GamePhase.SETUP.equals(gameStateDao.getGamePhase())) {
                handleSetupPhase(beacon_signal, robotId, robot);
            }
            if (com.shared.domain.GamePhase.EXPLORATION.equals(gameStateDao.getGamePhase())
                    && gameStateDao.getGameState().equals(com.shared.domain.GameState.RUNNING)
                    && !robotClient.isRobotsStopped()
                    && (System.currentTimeMillis() - robot.getTimeLastTaskAssignment()) > SLEEP_TASK_ASSIGNMENT) {
                handleExplorationPhase(beacon_signal, robotId, robot);
            }

            // if a robot sends a beacon signal in production, we assign him a task
            // if the robot has no task
            if (com.shared.domain.GamePhase.PRODUCTION.equals(gameStateDao.getGamePhase())
                    && gameStateDao.getGameState().equals(com.shared.domain.GameState.RUNNING)
                    && !robotClient.isRobotsStopped()
                    && (System.currentTimeMillis() - robot.getTimeLastTaskAssignment()) > SLEEP_TASK_ASSIGNMENT) { //todo this does not use the sim time!
                handleProductionPhase(beacon_signal, robotId, robot);
            }

            currentlyInAssignTask = false;
        }
    }

    private void handleSetupPhase(BeaconSignalProtos.BeaconSignal beacon_signal, int robotId, Peer robot) {
        log.info("Info:\nTeamColor: " + refboxConfig.getTeamcolor() + "\nCryptoKey: " + refboxConfig.getCrypto_key() +
                "\n" + "AspNumberRobots: " + numberRobots + "\nAspMinOrders" + minOrdersToPlan);
    }

    private void handleProductionPhase(BeaconSignalProtos.BeaconSignal beacon_signal, int robotId, Peer robot) {
        // check if required information was already received from refbox
        boolean startInExplo = false;
        if (productionScheduler instanceof GraspingScheduler) {
            this.productionScheduler.handleBeaconSignal(beacon_signal, robotId, robot);
        } else {
            if (startInExplo && machineInfoRefBoxDao.count() < 12) {
                explorationScheduler.handleBeaconSignal(beacon_signal, robotId, robot);
            } //if all found, start production!
            else if (dbService.checkProductionReady() && dbService.hasOrders()) {
                if (startInExplo && explorationScheduler.anyActiveTasks()) {
                    log.warn("Waiting that exploration scheduler tasks finish...");
                } else if (machineInfoRefBoxDao.count() >= 6) { //number of machines in our teamcolor!
                    productionScheduler.handleBeaconSignal(beacon_signal, robotId, robot);
                }
            } else {
                log.warn("Not yet ready for production!");
            }
        }
    }

    private void handleExplorationPhase(BeaconSignalProtos.BeaconSignal beacon_signal, int robotId, Peer robot) {
        explorationScheduler.handleBeaconSignal(beacon_signal, robotId, robot);
    }
}
