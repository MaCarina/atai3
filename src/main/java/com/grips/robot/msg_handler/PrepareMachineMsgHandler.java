package com.grips.robot.msg_handler;

import com.grips.persistence.misc.GripsDataService;
import com.rcll.domain.MachineClientUtils;
import com.rcll.domain.MachineName;
import com.grips.persistence.dao.ProductOrderDao;
import com.grips.persistence.dao.SubProductionTaskDao;
import com.grips.persistence.domain.ProductOrder;
import com.grips.persistence.domain.SubProductionTask;
import com.grips.persistence.domain.SubProductionTaskBuilder;
import com.grips.scheduler.exploration.PreProductionService;
import com.rcll.protobuf_lib.RobotMessageRegister;
import com.rcll.refbox.RefboxClient;
import com.robot_communication.services.GripsPrsTaskCreator;
import com.robot_communication.services.PrsTaskCreator;
import com.robot_communication.services.RobotClient;
import com.shared.domain.MachineSide;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.GripsPrepareMachineProtos;
import org.robocup_logistics.llsf_msgs.MachineInstructionProtos;
import org.robocup_logistics.llsf_utils.Key;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

@Service
@CommonsLog
public class PrepareMachineMsgHandler implements Consumer<GripsPrepareMachineProtos.GripsPrepareMachine> {
    private final RefboxClient refboxClient;
    private final SubProductionTaskDao subProductionTaskDao;
    private final GripsDataService<ProductOrder, Long> productOrderDao;
    private final GripsPrsTaskCreator prsTaskCreator;
    private final RobotClient robotClient;

    PreProductionService preProductionService;

    public PrepareMachineMsgHandler(RefboxClient refboxClient,
                                    SubProductionTaskDao subProductionTaskDao,
                                    ProductOrderDao productOrderDao,
                                    GripsPrsTaskCreator prsTaskCreator,
                                    RobotClient robotClient,
                                    PreProductionService preProductionService) {
        this.refboxClient = refboxClient;
        this.subProductionTaskDao = subProductionTaskDao;
        this.productOrderDao = productOrderDao;
        this.prsTaskCreator = prsTaskCreator;
        this.robotClient = robotClient;
        this.preProductionService = preProductionService;
    }

    @Override
    public void accept(GripsPrepareMachineProtos.GripsPrepareMachine prepareMachine) {
        log.info("Received PrepareMachine: " + prepareMachine.toString());
        Key key = RobotMessageRegister.getInstance().get_msg_key_from_class(MachineInstructionProtos.PrepareMachine.class);
        MachineName machineName = new MachineName(prepareMachine.getMachineId());
        List<SubProductionTask> tasks = this.subProductionTaskDao.findByRobotIdAndState(prepareMachine.getRobotId(), SubProductionTask.TaskState.INWORK);
        if (tasks.size() != 1) {
            log.warn("No SubProduction Task Found, trying in PreProduction");
            machineName = preProductionService.getPrepare((long) prepareMachine.getRobotId());
            tasks = Collections.singletonList(SubProductionTaskBuilder.newBuilder()
                    .setOptCode("RETRIEVE_CAP")
                    .build());
            if (machineName == null) {
                throw new RuntimeException(tasks.size() + " INWORK tasks found for robot: " + prepareMachine.getRobotId());
            }
        }
        if (machineName.isCapStation()) {
            prepareCapStation(machineName, prepareMachine.getRobotId(),
                    MachineClientUtils.CSOp.valueOf(tasks.get(0).getOptCode()));
        } else if (machineName.isBaseStation()) {
            prepareBaseStation(tasks.get(0));
        } else if (machineName.isRingStation()) {
            prepareRingStation(machineName, tasks.get(0));
        } else if (machineName.isDeliveryStation()) {
            prepareDeliveryStation(tasks.get(0));
        } else {
            throw new RuntimeException("Unsupported Machine: " + machineName.toString());
        }
        log.info("Received PrepareMachine for machine " + prepareMachine.getMachineId() + "from robot: " + prepareMachine.getRobotId() + ".");    // TODO: From robot?
    }

    private void prepareDeliveryStation(SubProductionTask task) {
        ProductOrder pOrder = productOrderDao.findById(task.getOrderInfoId()).get();
        this.refboxClient.sendPrepareDS(new Long(pOrder.getDeliveryGate()).intValue(), new Long(pOrder.getId()).intValue());
        GripsPrepareMachineProtos.GripsPrepareMachine success = prsTaskCreator.createPrepareMachineTask(task.getRobotId().longValue(), task.getMachine(), "input");
        robotClient.sendToRobot(task.getRobotId(), success);
    }

    private void prepareRingStation(MachineName machineName, SubProductionTask task) {
        this.refboxClient.sendPrepareRS(machineName.asMachineEnum(), toRefboxRingColor(task.getRequiredColor()));
        GripsPrepareMachineProtos.GripsPrepareMachine success = prsTaskCreator.createPrepareMachineTask(task.getRobotId().longValue(), task.getMachine(), "input");
        robotClient.sendToRobot(task.getRobotId(), success);
    }

    private void prepareBaseStation(SubProductionTask task) {
        this.refboxClient.sendPrepareBS(task.getSide() == MachineSide.INPUT ? MachineClientUtils.MachineSide.Input : MachineClientUtils.MachineSide.Output, MachineClientUtils.BaseColor.valueOf(task.getRequiredColor()));
        GripsPrepareMachineProtos.GripsPrepareMachine success = prsTaskCreator.createPrepareMachineTask(task.getRobotId().longValue(), task.getMachine(), task.getSide().toString());
        robotClient.sendToRobot(task.getRobotId(), success);
    }

    private void prepareCapStation(MachineName machineName, long robotId, MachineClientUtils.CSOp optCode) {
        this.refboxClient.sendPrepareCS(machineName.asMachineEnum(), optCode);
        GripsPrepareMachineProtos.GripsPrepareMachine success = prsTaskCreator.createPrepareMachineTask(
                robotId, machineName.getRawMachineName(), "input");
        robotClient.sendToRobot(robotId, success);
    }

    private MachineClientUtils.RingColor toRefboxRingColor(String requiredColor) {
        switch (requiredColor) {
            case "Yellow":
                return MachineClientUtils.RingColor.Yellow;
            case "Blue":
                return MachineClientUtils.RingColor.Blue;
            case "Green":
                return MachineClientUtils.RingColor.Green;
            case "Orange":
                return MachineClientUtils.RingColor.Orange;
        }
        throw new IllegalArgumentException("Unkown ring Color: " + requiredColor);
    }
}
