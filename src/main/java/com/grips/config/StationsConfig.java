package com.grips.config;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "stations")
@Getter
@Setter
public class StationsConfig {
    private boolean useCs1;
    private boolean useCs2;
    private boolean useRs1;
    private boolean useRs2;
    private boolean useDs;
}
