package com.grips.refbox.msg_handler;

import com.grips.FakeDozerMapper;
import com.grips.FakeGripsDataService;
import com.grips.persistence.domain.ProductOrder;
import org.junit.jupiter.api.Test;
import org.robocup_logistics.llsf_msgs.OrderInfoProtos;
import org.robocup_logistics.llsf_msgs.ProductColorProtos;

import static org.mockito.Mockito.mock;

class OrderInfoHandlerTest {
    @Test
    public void testHandleOrder() {
        OrderInfoProtos.OrderInfo order = OrderInfoProtos.OrderInfo.newBuilder()
                .addOrders(OrderInfoProtos.Order.newBuilder()
                        .setId(1)
                        .setCapColor(ProductColorProtos.CapColor.CAP_BLACK)
                        .setBaseColor(ProductColorProtos.BaseColor.BASE_SILVER)
                        .addRingColors(ProductColorProtos.RingColor.RING_BLUE)
                        .addRingColors(ProductColorProtos.RingColor.RING_GREEN)
                        .addRingColors(ProductColorProtos.RingColor.RING_YELLOW)
                        .setCompetitive(true)
                        .setComplexity(OrderInfoProtos.Order.Complexity.C3)
                        .setDeliveryGate(1)
                        .setDeliveryPeriodBegin(1000)
                        .setDeliveryPeriodEnd(2000)
                        .setQuantityDeliveredCyan(1)
                        .setQuantityDeliveredMagenta(2)
                        .setQuantityRequested(3)
                        .build())
                .build();
        FakeGripsDataService<ProductOrder, Long> productOrderDao = new FakeGripsDataService<>();
        OrderInfoHandler orderInfoHandler = new OrderInfoHandler(FakeDozerMapper.getMapper(), productOrderDao);
        orderInfoHandler.accept(order);
    }
}