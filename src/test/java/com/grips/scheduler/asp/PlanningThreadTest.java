package com.grips.scheduler.asp;

import com.grips.config.GameFieldConfig;
import com.grips.config.ProductionConfig;
import com.grips.config.ProductionTimesConfig;
import com.grips.config.RefboxConfig;
import com.grips.persistence.dao.AtomDao;
import com.grips.persistence.dao.MachineInfoRefBoxDao;
import com.grips.persistence.domain.MachineInfoRefBox;
import com.grips.persistence.domain.WorldKnowledge;
import com.grips.tools.PathEstimator;
import com.rcll.domain.*;
import com.rcll.refbox.RefboxClient;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class PlanningThreadTest {
    @Test
    public void testPlanning() {
        ProductionTimesConfig productionTimesConfig = new ProductionTimesConfig();
        productionTimesConfig.setBufferTask(50);
        productionTimesConfig.setNormalTask(50);
        productionTimesConfig.setMoveTask(50);

        RefboxClient refboxClient = mock(RefboxClient.class);
        Map<Integer, Order> orders = new HashMap<>();
        orders.put(1, Order.builder()
                .id(1)
                .base(Base.Black)
                .cap(Cap.Grey)
                .ring1(new Ring(MachineClientUtils.Machine.RS1, MachineClientUtils.RingColor.Green, 1))
                .competitive(false)
                .delivered(0)
                .deliveryPeriodBegin(500)
                .deliveryPeriodEnd(1500)
                .requested(1)
                .build());
        orders.put(2, Order.builder()
                .id(2)
                .base(Base.Silver)
                .cap(Cap.Grey)
                .competitive(false)
                .delivered(0)
                .deliveryPeriodBegin(500)
                .deliveryPeriodEnd(1500)
                .requested(1)
                .build());
        orders.put(3, Order.builder().id(3)
                .base(Base.Red)
                .cap(Cap.Grey)
                .competitive(false)
                .delivered(0)
                .deliveryPeriodBegin(500)
                .deliveryPeriodEnd(1500)
                .requested(1)
                .build());
        orders.put(4, Order.builder()
                .id(4)
                .base(Base.Red)
                .cap(Cap.Grey)
                .competitive(false)
                .delivered(0)
                .deliveryPeriodBegin(500)
                .deliveryPeriodEnd(1500)
                .requested(1)
                .build());
        when(refboxClient.getAllOrders()).thenReturn(new ArrayList<>(orders.values()));
        when(refboxClient.getOrderById(eq(1))).thenReturn(orders.get(1));
        when(refboxClient.getOrderById(eq(2))).thenReturn(orders.get(2));
        when(refboxClient.getOrderById(eq(3))).thenReturn(orders.get(3));
        when(refboxClient.getOrderById(eq(4))).thenReturn(orders.get(4));
        when(refboxClient.getRingByColor(eq(MachineClientUtils.RingColor.Blue))).thenReturn(new Ring(MachineClientUtils.Machine.RS1, MachineClientUtils.RingColor.Blue, 0));
        when(refboxClient.getRingByColor(eq(MachineClientUtils.RingColor.Green))).thenReturn(new Ring(MachineClientUtils.Machine.RS1, MachineClientUtils.RingColor.Green, 1));
        when(refboxClient.getRingByColor(eq(MachineClientUtils.RingColor.Yellow))).thenReturn(new Ring(MachineClientUtils.Machine.RS2, MachineClientUtils.RingColor.Yellow, 0));
        when(refboxClient.getRingByColor(eq(MachineClientUtils.RingColor.Orange))).thenReturn(new Ring(MachineClientUtils.Machine.RS2, MachineClientUtils.RingColor.Orange, 2));
        when(refboxClient.getLatestGameTimeInSeconds()).thenReturn(125L);
        AtomDao atomDao = new FakeAtomDao();
        MachineInfoRefBoxDao machineInfoRefBoxDao = mock(MachineInfoRefBoxDao.class);
        List<MachineInfoRefBox> refboxMachines = Arrays.asList(
                MachineInfoRefBox.builder()
                        .name("C-CS1")
                        .zone("C-Z11")
                        .type("CS")
                        .build(),
                MachineInfoRefBox.builder()
                        .name("C-CS2")
                        .zone("C-Z22")
                        .type("CS")
                        .build(),
                MachineInfoRefBox.builder()
                        .name("C-RS1")
                        .zone("C-Z33")
                        .type("RS")
                        .ring1(MachineClientUtils.RingColor.Blue)
                        .ring2(MachineClientUtils.RingColor.Green)
                        .build(),
                MachineInfoRefBox.builder()
                        .name("C-RS2")
                        .zone("C-Z44")
                        .type("RS")
                        .ring1(MachineClientUtils.RingColor.Yellow)
                        .ring2(MachineClientUtils.RingColor.Orange)
                        .build(),
                MachineInfoRefBox.builder()
                        .name("C-DS")
                        .zone("C-Z55")
                        .type("DS")
                        .build(),
                MachineInfoRefBox.builder()
                        .name("C-BS")
                        .zone("C-Z31")
                        .type("BS")
                        .build()
        );
        when(machineInfoRefBoxDao.findByTeamColor(any())).thenReturn(refboxMachines);
        when(machineInfoRefBoxDao.findByName(eq("C-CS1"))).thenReturn(refboxMachines.get(0));
        when(machineInfoRefBoxDao.findByName(eq("C-CS2"))).thenReturn(refboxMachines.get(1));
        when(machineInfoRefBoxDao.findByName(eq("C-RS1"))).thenReturn(refboxMachines.get(2));
        when(machineInfoRefBoxDao.findByName(eq("C-RS2"))).thenReturn(refboxMachines.get(3));
        when(machineInfoRefBoxDao.findByName(eq("C-DS"))).thenReturn(refboxMachines.get(4));
        when(machineInfoRefBoxDao.findByName(eq("C-BS"))).thenReturn(refboxMachines.get(5));
        ProductionConfig productionConfig = new ProductionConfig();
        productionConfig.setDoC0(true);
        productionConfig.setDoC1(true);
        productionConfig.setDoC2(true);
        productionConfig.setDoC3(true);
        productionConfig.setBlack_cap_machine("CS1");
        productionConfig.setGrey_cap_machine("CS2");
        KnowledgeBase kb = new KnowledgeBase(atomDao, machineInfoRefBoxDao, productionConfig, refboxClient);
        //kb.resetKnowledgeBase(new WorldKnowledge());
        //kb.generateInitialKB();
        WorldKnowledge worldKnowledge = new WorldKnowledge();
        worldKnowledge.setCs1Buffered(false);
        worldKnowledge.setCs2Buffered(false);
        //kb.resetKnowledgeBase(worldKnowledge);


        Map<String, Double> distanceMatrix = mock(Map.class);
        when(distanceMatrix.get(any())).thenReturn(10d);

        //PlanningThread planningThread = new PlanningThread("1", false, 1000,
         //       Collections.emptyList(), false, false, kb, 3, atomDao,
         //       productionTimesConfig, machineInfoRefBoxDao, "CYAN", distanceMatrix, refboxClient);
        //planningThread.run();
    }
}
