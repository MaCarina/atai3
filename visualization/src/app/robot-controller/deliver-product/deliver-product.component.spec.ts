import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliverProductComponent } from './deliver-product.component';

describe('DeliverProductComponent', () => {
  let component: DeliverProductComponent;
  let fixture: ComponentFixture<DeliverProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeliverProductComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliverProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
