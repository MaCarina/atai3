import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BufferCapComponent } from './buffer-cap.component';

describe('BufferCapComponent', () => {
  let component: BufferCapComponent;
  let fixture: ComponentFixture<BufferCapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BufferCapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BufferCapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
