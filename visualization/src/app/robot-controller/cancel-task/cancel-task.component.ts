import { Component, OnInit } from '@angular/core';
import {FormGroup} from "@angular/forms";
import {formly} from "ngx-formly-helpers";
import {RobotOptions} from "../robots";
import {MachineOptions} from "../machines";
import {MachinePointOptions} from "../machine-points";
import {EndpointService} from "../../services/endpoint.service";

@Component({
  selector: 'app-cancel-task',
  templateUrl: './cancel-task.component.html',
  styleUrls: ['./cancel-task.component.sass']
})
export class CancelTaskComponent implements OnInit {

  form = new FormGroup({});
  model: any = {};
  fields: any[] = [
    formly.requiredSelect("robotId", "Robot ID", RobotOptions)
  ];

  constructor(private endpoint: EndpointService) { }

  ngOnInit(): void {
  }

  public submit() {
    console.log("Model is: ", this.model);
    this.endpoint.post("robot-controller/cancel-task", this.model)
      .subscribe(res => console.log("Cancel task send!"));
  }

}
