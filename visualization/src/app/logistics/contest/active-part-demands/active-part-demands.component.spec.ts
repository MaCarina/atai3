import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivePartDemandsComponent } from './active-part-demands.component';

describe('ActivePartDemandsComponent', () => {
  let component: ActivePartDemandsComponent;
  let fixture: ComponentFixture<ActivePartDemandsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivePartDemandsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivePartDemandsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
