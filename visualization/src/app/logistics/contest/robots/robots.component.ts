import {Component, Input, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-robots',
  templateUrl: './robots.component.html',
  styleUrls: ['./robots.component.sass']
})
export class RobotsComponent implements OnInit {

  @Input() robots: any[];
  allTasks = ['GetBaseFromShelf', 'GetBaseFromMachine', 'GetBaseFromCS',
              'DeliverBaseToCS', 'DeliverBaseToDS', 'DeliverBaseToMachine',
              'MoveToZone'];
  curTask: any;
  machine: any;
  robotID: any;
  robotColor: any;
  zone: any;

  constructor(private httpClient:HttpClient) { }

  ngOnInit() {
    this.curTask = "Task..."
    console.log("Robots value: ", this.robots);
  }

  setTask($event) {
    console.log("Task to teamserver ", $event);
    this.curTask = $event.value;
  }

  setMachine($event) {
    this.machine = $event.value;
  }

  setRobotID($event) {
    this.robotID = $event.value;
  }

  setZone($event){
    this.zone = $event.value;
  }

  sendTask() {
    //Base checks of the parameters
    if (this.robotID == null) {
      alert("Robot Id must be set!");
      return;
    }
    if (this.curTask == null) {
      alert("Please set a task for the Robot!");
      return;
    }

    //Checking tasks
    if (this.curTask != "MoveToZone" && this.machine == null) {
      alert("For tasks involving a machine please set a machine!);");
      return;
    }
    if (this.curTask == "MoveToZone" && this.zone == null)
    {
      alert("For tasks involving a zone please set a zone! (M/C_Zxy);");
      return;
    }

    this.httpClient.post("http://localhost:8090/send_robot_task", {task: this.curTask, robotID: this.robotID,
                                                                             machineForTask: this.machine, zone: this.zone, robotColor: this.robots[0].teamColor})
      .subscribe(res=>{
        console.log(res);
      });
    console.log("send task");
  }

}
