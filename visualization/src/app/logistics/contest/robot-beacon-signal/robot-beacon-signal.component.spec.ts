import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RobotBeaconSignalComponent } from './robot-beacon-signal.component';

describe('RobotBeaconSignalComponent', () => {
  let component: RobotBeaconSignalComponent;
  let fixture: ComponentFixture<RobotBeaconSignalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RobotBeaconSignalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RobotBeaconSignalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
